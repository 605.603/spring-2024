package com.javadude.week1102

class Actor
class Movie
class Theater

fun fetchActors(): List<Actor> {
    return listOf()
}
fun fetchMoviesForActor(actor: Actor): List<Movie> {
    return listOf()
}
fun fetchTheatersForMovie(movie: Movie): List<Movie> {
    return listOf()
}


fun main() {
    val actors = fetchActors()
    actors.forEach { actor ->
        val movies = fetchMoviesForActor(actor)
        movies.forEach { movie ->
            val theaters = fetchTheatersForMovie(movie)
            // print something
        }
    }
}