package com.javadude.week1104

// background processing

//  Cooperative - older
//      each application must SHARE processing EXPLICITLY
//          must yield explicitly to allow other apps to share processor

//  Preemptive
//      computer time-slices, swap app and data in and out automatically

// Coroutines
//      "lightweight threads"
//      cooperative
//          Kotlin makes this easy!
//          Every time a suspend function is called, coroutine yields automatically

suspend fun a() {}
suspend fun b() {}
suspend fun c() {}

fun x() {}
fun y() {}
fun z() {}

suspend fun caller(x: Int): Int {
    x()
    y()
    a()
    z()
    b()
    c()
    return 10
}

// converted into something like (not exact but useful for description)
//suspend fun caller(x: Int, continuation: Continuation<Int>) {
//    when (continuation.blockToRun) {
//        1 -> {
//            // load data from continuation
//            x()
//            y()
//            // save data in continuation
//            // set continuation blockToRun to 2
//        }
//        2 -> {
//            // load data from continuation
//            a()
//            z()
//            // save data in continuation
//            // set continuation blockToRun to 3
//        }
//        3 -> {
//            // load data from continuation
//            b()
//            // save data in continuation
//            // set continuation blockToRun to 4
//        }
//        4 -> {
//            // load data from continuation
//            c()
//            // return result
//        }
//    }
//}




fun main() {
}