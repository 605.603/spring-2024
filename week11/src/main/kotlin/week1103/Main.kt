package com.javadude.week1103

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class Actor
class Movie
class Theater

// suspending function
fun fetchActorsX1(): List<Actor> = listOf()
fun fetchActorsX2(): List<Actor> {
    return listOf()
}

// "Main-Safe"
suspend fun fetchActors(): List<Actor> = withContext(Dispatchers.IO) {
    // doing heavy processing (without calling suspend functions), be sure to call ensureActive()
    ensureActive()
//    if (isActive) {
//
//    }
    listOf()
}
suspend fun fetchMoviesForActor(actor: Actor): List<Movie> = withContext(Dispatchers.IO) {
    listOf()
}
suspend fun fetchTheatersForMovie(movie: Movie): List<Movie> = withContext(Dispatchers.IO) {
    listOf()
}


fun main() {
    val context = CoroutineName("My Context") + Dispatchers.IO
    val scope = CoroutineScope(context)

    scope.launch {
        val actors = async { fetchActors() }.await()
        actors.forEach { actor ->
            launch {
                val movies = async { fetchMoviesForActor(actor) }.await()
                movies.forEach { movie ->
                    launch {
                        val theaters = async { fetchTheatersForMovie(movie) }.await()
                        // print something
                    }
                }
            }
            // do something else (in parallel with the above "launch")
        }
    }
}