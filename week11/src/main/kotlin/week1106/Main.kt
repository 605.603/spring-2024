package com.javadude.week1106

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.WindowConstants

data class Movie(
    val id: String,
    val title: String,
    val year: Int,
)

// FLOW-BASED APPROACH

class MovieDAO {
    private val movies = mutableMapOf(
        "terminator" to Movie("terminator", "The Terminator", 1984),
        "transporter" to Movie("transporter", "The Transporter", 2002),
        "spiderverse" to Movie("spiderverse", "Into The Spiderverse", 2018),
    )

    private val _moviesFlow = MutableStateFlow(movies.values.toList())
    val moviesFlow: Flow<List<Movie>> = _moviesFlow

    suspend fun update(movie: Movie) {
        movies[movie.id] = movie
        _moviesFlow.value = movies.values.toList()
    }
}

// Null Object pattern
private val nullMovie = Movie("","",-1)

class ViewModel {
    val coroutineContext = CoroutineName("View Model") + Dispatchers.Default
    val viewModelScope = CoroutineScope(coroutineContext)

    var onChanged: () -> Unit = {}
    private val dao = MovieDAO()

    val moviesFlow = dao.moviesFlow

    private val _selectedMovieFlow = MutableStateFlow(nullMovie)
    val selectedMovieFlow: Flow<Movie> = _selectedMovieFlow

    fun select(movie: Movie) {
        _selectedMovieFlow.value = movie
    }

    fun update(movie: Movie) {
        viewModelScope.launch {
            dao.update(movie)
        }
    }
}

fun main() {
    val coroutineContext = CoroutineName("Main") + Dispatchers.Default
    val coroutineScope = CoroutineScope(coroutineContext)

    val movieButtons = JPanel(GridLayout(0, 1, 10, 10))
    val movieDetails = JLabel("")

    val viewModel = ViewModel()

    coroutineScope.launch {
        viewModel.moviesFlow.collect { movies ->
            movieButtons.removeAll()
            movies.forEach {  movie ->
                movieButtons.add(JButton("<html>${movie.title}<br/>${movie.year}").apply {
                    addActionListener {
                        viewModel.update(movie.copy(year = movie.year + 1))
                        viewModel.select(movie)
                    }
                })
            }
            movieButtons.revalidate()
        }
    }

    coroutineScope.launch {
        viewModel.selectedMovieFlow.collect { selectedMovie ->
            // call XXXX - runs on a thread managed by Dispatchers.Default
            withContext(Dispatchers.Main) {
                    // on other platforms like Android, you may need to do updates on the UI thread
                movieDetails.text = selectedMovie.toString()
            }
            // call YYYY - runs on a thread managed by Dispatchers.Default
            //             - not guaranteed to run on the same thread as XXXX
            //             DON'T USE THREAD LOCALS (Java thing) TO HOLD DATA
        }
    }

    JFrame().apply {
        layout = BorderLayout()
        add(BorderLayout.WEST, movieButtons)
        add(BorderLayout.CENTER, movieDetails)
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        size = Dimension(600, 400)
        isVisible = true
    }

    // whenever any non-daemon thread is running in the JVM, the process is alive
    // if ONLY daemon threads are left, the process is killed by the JVM
}