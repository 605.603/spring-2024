package com.javadude.week1101

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.WindowConstants

fun main() {
    JFrame().apply {
        layout = BorderLayout()
        val label = JLabel("")
        val button = JButton("Press Me1").apply {
            addActionListener {
                Thread {
                    try {
                        isEnabled = false
                        (1..100).forEach {
                            label.text = it.toString()
                            Thread.sleep(20)
                        }
                    } finally {
                        isEnabled = true
                    }
                }.start()
            }
        }
        val button2 = JButton("Press me too!").apply {
            var n = 0
            addActionListener {
                n++
                text = "Pressed $n"
            }
        }
        add(BorderLayout.NORTH, button)
        add(BorderLayout.SOUTH, button2)
        add(BorderLayout.CENTER, label)

        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        size = Dimension(400, 200)
        isVisible = true
    }
}