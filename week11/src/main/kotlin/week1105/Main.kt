package com.javadude.week1105

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.WindowConstants

data class Movie(
    val id: String,
    val title: String,
    val year: Int,
)

// OBSERVER-BASED APPROACH

class MovieDAO(
    val onChanged: () -> Unit,
) {
    private val _movies = mutableMapOf(
        "terminator" to Movie("terminator", "The Terminator", 1984),
        "transporter" to Movie("transporter", "The Transporter", 2002),
        "spiderverse" to Movie("spiderverse", "Into The Spiderverse", 2018),
    )

    val movies: List<Movie>
        get() = _movies.values.toList()

    fun update(movie: Movie) {
        _movies[movie.id] = movie
        onChanged()
    }
}

// Null Object pattern
private val nullMovie = Movie("","",-1)

class ViewModel {
    var onChanged: () -> Unit = {}
    private val dao = MovieDAO { onChanged() }

    val movies: List<Movie>
        get() = dao.movies

//    var selectedMovie: Movie? = null
    var selectedMovie: Movie = nullMovie

    fun update(movie: Movie) {
        dao.update(movie)
    }
}

// demonstrate null-object use - not used in actual UI
fun someSampleUserInterfaceFunction(viewModel: ViewModel) {
    // display the selected movie
//    val titleToDisplay = viewModel.selectedMovie?.title ?: ""
    // if we use Null object instead of actual null, we don't need ?. or elvis
    val titleToDisplay = viewModel.selectedMovie.title
    val yearToDisplay = viewModel.selectedMovie.year.toString()
}


fun main() {
    val movieButtons = JPanel(GridLayout(0, 1, 10, 10))
    val movieDetails = JLabel("")

    val viewModel = ViewModel()

    fun updateMovies() {
        movieButtons.removeAll()
        viewModel.movies.forEach {  movie ->
            movieButtons.add(JButton("<html>${movie.title}<br/>${movie.year}").apply {
                addActionListener {
                    viewModel.update(movie.copy(year = movie.year + 1))
                    viewModel.selectedMovie = movie
                    movieDetails.text = viewModel.selectedMovie.toString()
                }
            })
        }
        movieButtons.revalidate()
    }

    viewModel.onChanged = ::updateMovies

    updateMovies() // first setup

    JFrame().apply {
        layout = BorderLayout()
        add(BorderLayout.WEST, movieButtons)
        add(BorderLayout.CENTER, movieDetails)
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        size = Dimension(600, 400)
        isVisible = true
    }
}