package javastuff;

import java.util.List;

public class Example {
    public static void main(String[] args) {
        ActorFinder actorFinder = new ActorFinder();
        actorFinder.fetchActors(new ResultHandler<>() {
            // THIS IS CALLBACK HELL
            @Override
            public void onSuccess(List<Actor> results) {
                results.forEach((actor) -> {
                    actorFinder.fetchMoviesForActor(actor, new ResultHandler<>() {
                        @Override
                        public void onSuccess(List<Movie> results) {
                            results.forEach((movie) -> {
                                actorFinder.fetchTheatersForMovie(movie, new ResultHandler<Theater>() {
                                    @Override
                                    public void onSuccess(List<Theater> results) {
                                        // print out the theaters
                                    }

                                    @Override
                                    public void onFailure() {

                                    }
                                });
                            });
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
                });
            }

            @Override
            public void onFailure() {

            }
        });
    }
}
