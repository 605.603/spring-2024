package javastuff;

import java.util.ArrayList;
import java.util.List;

class Actor {}
class Movie {}
class Theater {}

interface ResultHandler<T> {
    void onSuccess(List<T> results);
    void onFailure();
}

public class ActorFinder {
    //  fetch list of actors
    //  for each actor
    //      fetch movies with those actors
    //      for each movie
    //          fetch where you can watch them

    void fetchActors(ResultHandler<Actor> result) {
        // pretend I'm starting a thread to fetch all actors
        List<Actor> actors = new ArrayList<>();
        result.onSuccess(actors);
    }
    void fetchMoviesForActor(Actor actor, ResultHandler<Movie> result) {
        // pretend I'm starting a thread to fetch movies starring an actor
        List<Movie> movies = new ArrayList<>();
        result.onSuccess(movies);
    }
    void fetchTheatersForMovie(Movie movie, ResultHandler<Theater> result) {
        // pretend I'm starting a thread to fetch theaters showing a movie
        List<Theater> theaters = new ArrayList<>();
        result.onSuccess(theaters);
    }
}
