package com.javadude.week011

const val name3 = "Scott"

fun main(args: Array<String>) {
    val name1: String = "Scott" // String is redundant
    val name2 = "Scott" // can remove it

    val first = "Hello"
    val last = "World"
    val helloWorld = first + ", " + last // basic string concatenation

    println(helloWorld)

    val helloWorldUsingTemplate1 = "$first, $last" // basic string template
    val helloWorldUsingTemplate2 = "${first.substring(0, 2)}, ${last.substring(0, 2)}" // basic string template

    val typeOfThings = "animal"
    val numberOfThings = 10
    val onlyOne = 1
    println(helloWorldUsingTemplate2)

    val message1 = "There are $numberOfThings ${typeOfThings}s"
    val message2 = "There is $onlyOne $typeOfThings"

    println(message1)
    println(message2)

    // RAW Strings
    val message3 = """
        Copyright 2024 Scott Stanchfield
        All Rights Reserved
        
        Some other license text here
    """.trimIndent()

    println(message3)

    val message4 = """
        |Copyright 2024 Scott Stanchfield
        |               All Rights Reserved
        |
        |Some other license text here
    """.trimMargin()
    println(message4)

    val message5 = """
        Copyright 2024 Scott Stanchfield
                       All Rights Reserved
        
        Some other license text here
    """.trimIndent()
    println(message5)
}
