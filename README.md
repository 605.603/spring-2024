# 605.603 Spring 2024

## Class Videos

   * 20240125: https://youtu.be/vHTzVHHivks
   * 20240201: https://youtu.be/LhW1-CcNkYg
   * 20240208
     * Video: https://youtu.be/OgI5FFmiu2M
     * Article: https://www.javadude.com/posts/20190203-kotlin01/
   * 20240215: Instructor sick. Please watch
     * https://youtu.be/y0v_-jsP_FI?feature=shared&t=3052 (only from  50:52 to 1:12:26)
     * https://youtu.be/jYatU5KU1Rw?feature=shared&t=38 (full video)
   * 20240222: 
     * Video: https://youtu.be/NMdjmJrnZqY
     * Article on using asSequence() or not: https://typealias.com/guides/when-to-use-sequences/
   * 20240229: https://youtu.be/b85p4e4XdsY
   * 20240314: https://youtu.be/TJ2Z93pYb5A
   * 20240328: Instructor sick. Please watch
     * https://www.youtube.com/watch?v=8_F0svoi0nA
     * Dave Leeds' short videos on Variance and type projections. Some of his explanations in these 10-ish minute videos are super helpful and should help make some of concepts I covered more clear.
        * https://www.youtube.com/watch?v=6moaoAJui_4
        * https://www.youtube.com/watch?v=Yz0xBaFt50s
   * 20240404: https://youtu.be/6UXPyElzLQ8
   * 20240411: https://youtu.be/gselTGpRVkM
   * 20240418: https://youtu.be/uDCdXbZuDP4
   * 20240425: https://youtu.be/4ESP4eiIur8
   
