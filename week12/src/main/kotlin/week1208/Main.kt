package com.javadude.week1208

class Person(val id: String)
class Animal(val id: String)

fun Person.doStuff(block: Person.() -> Unit) {
    block()
}
fun Animal.doStuff(block: Animal.() -> Unit) {
    block()
}

fun main() {
    // TODO example using this@
}

