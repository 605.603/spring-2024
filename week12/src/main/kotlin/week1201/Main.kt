package com.javadude.week1201

// More fun with properties

// LATEINIT PROPERTIES

// pretend we have an XML file that describes the user interface

interface Widget
class TextWidget: Widget
class ButtonWidget: Widget {
    var onClick: () -> Unit = {} // null object pattern
}

class XMLUIFile {
    fun findWidgetFor(id: String): Widget {
        return ButtonWidget() // would actually look it up and return the appropriate type
    }
}

class MyUserInterface {
    var button: ButtonWidget? = null
    var text: TextWidget? = null

    // called by the UI framework itself
    fun onUserInterfaceSetup(xmlFile: XMLUIFile) {
        button = xmlFile.findWidgetFor("Press Me") as ButtonWidget // throw exception if wrong type
        button = xmlFile.findWidgetFor("Press Me") as? ButtonWidget // return null if wrong type
        button = xmlFile.findWidgetFor("Press Me") as? ButtonWidget ?: ButtonWidget() // gives option for non-null setup

        button?.onClick = {}
    }
}

// using lateinit variable
//    promises the compiler that a variable will be assigned before it's read
//      - compiler will trust but verify
// only use it when REALLY safe to do so

class MyUserInterface2 {
    lateinit var button: ButtonWidget
    lateinit var text: TextWidget

    // called by the UI framework itself
    fun onUserInterfaceSetup(xmlFile: XMLUIFile) {
        button = xmlFile.findWidgetFor("Press Me") as ButtonWidget // throw exception if wrong type
//        button = xmlFile.findWidgetFor("Press Me") as? ButtonWidget // CAN'T return null if wrong type
        button = xmlFile.findWidgetFor("Press Me") as? ButtonWidget
            ?: throw IllegalStateException("Press Me is either not found, or not a ButtonWidget")
        button = xmlFile.findWidgetFor("Press Me") as? ButtonWidget ?: ButtonWidget() // gives option for non-null setup

        button.onClick = {}
    }


}