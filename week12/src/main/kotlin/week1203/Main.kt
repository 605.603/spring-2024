package com.javadude.week1203

import kotlin.reflect.KProperty

class LoggingDelegate<PROPERTY_TYPE>(private var value: PROPERTY_TYPE) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): PROPERTY_TYPE {
        println("Property ${property.name} is being read")
        return value
    }
    operator fun setValue(thisRef: Any?, property: KProperty<*>, newValue: PROPERTY_TYPE) {
        println("Property ${property.name} is being set to $newValue")
        this.value = newValue
    }
}

class Person {
    var name by LoggingDelegate("Scott")
    var age by LoggingDelegate(57)
}

fun main() {
    val person = Person()
    println(person.name)
    println(person.age)
    person.name = "Humuhumunukunukuapua'a"
    println(person.name)
}

// what if I still wanted to require name and age
// to be passed in when creating a Person?

class Person2(
    name: String,
    age: Int,
) {
    var name by LoggingDelegate("Scott")
    var age by LoggingDelegate(57)

    init {
        this.name = name
        this.age = age
    }
}

// see https://www.javadude.com/posts/20190203-kotlin01/
//    for a nice description on constructors and secondary constructors etc