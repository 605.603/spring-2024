package com.javadude.week1202

import kotlin.reflect.KProperty

// PROPERTY DELEGATION

interface Foo {
    fun a()
    fun b()
}

class FooImpl: Foo {
    override fun a() {
        TODO("Not yet implemented")
    }

    override fun b() {
        TODO("Not yet implemented")
    }
}

// "by" provides delegation for classes, objects or properties
class MyFoo1: Foo by FooImpl()
class MyFoo2(impl: FooImpl): Foo by impl


class Person(
    val name: String,
    val age: Int,
) {
    val description1: String
        get() = "$name: $age" // evaluates every time it's used

    val description2: String = "$name: $age"
        // only evaluates exactly once
        // if name or age were mutable, this would never update

    val description3 by lazy {
        "$name: $age"
    }

    // lazy {...} creates an object to hold a factory (lambda)

    private fun computeLazyValue() = "$name: $age" // the lambda
    private var descriptionValue: String? = null
    val description4: String
        get() {
            // lazy instantiation pattern
            if (descriptionValue == null) {
                descriptionValue = computeLazyValue()
            }
            return descriptionValue!!
        }

    // custom lazy instantiation delegate
    val description5 by MyLazy {
        "$name: $age"
    }
    val description6 by myLazy {
        "$name: $age"
    }

    val name2 by myLazy2 { "Scott" }
    val age2 by myLazy2 { 57 }
}

fun myLazy(computeLazyValue: () -> String) =
    MyLazy(computeLazyValue)

class MyLazy(val computeLazyValue: () -> String) {
    private var value: String? = null
    operator fun getValue(thisRef: Person, property: KProperty<*>): String {
        if (value == null) {
            value = computeLazyValue()
        }
        return value!!
    }
}

fun <PROPERTY_TYPE> myLazy2(computeLazyValue: () -> PROPERTY_TYPE) =
    MyLazy2(computeLazyValue)

class MyLazy2<PROPERTY_TYPE>(val computeLazyValue: () -> PROPERTY_TYPE) {
    private var value: PROPERTY_TYPE? = null
    operator fun getValue(thisRef: Any, property: KProperty<*>): PROPERTY_TYPE {
        if (value == null) {
            value = computeLazyValue()
        }
        return value!!
    }
}
