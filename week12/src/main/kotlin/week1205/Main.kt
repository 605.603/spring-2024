package com.javadude.week1205

// inline functions

fun main() {
    val list = listOf(1,2,3)
    foo1(list)
    foo2a(list)
}

fun foo1(list: List<Int>) {
    list.myForEach1 {
        if (it % 2 == 0) {
            return@myForEach1 // non-local return
        }
    }
}

fun foo2(list: List<Int>) {
    list.myForEach2 {
        if (it % 2 == 0) {
            return // returns from foo2
        }
    }

    // exactly the same as
    for (item in list) {
        if (item % 2 == 0) {
            return // returns from foo2
        }
    }
}
fun foo2a(list: List<Int>) {
    list.myForEach2 {
        if (it % 2 == 0) {
            return@myForEach2 // returns from myForEach2
        }
    }

    println("break happened")

    // equivalent to
    myForEach2@for (item in list) {
        if (item % 2 == 0) {
            break@myForEach2
        }
    }
}

fun foo3(list: List<Int>) {
    list.myForEach3 {
        if (it % 2 == 0) {
            return@myForEach3
        }
    }

    println("break happened")

    // kinda equivalent to
    // lambda created as object and called
    val stuffToDo = { item: Int ->
        if (item % 2 != 0) {
            // do stuff
        }
    }
    for (item in list) {
        stuffToDo(item)
    }

    list.myForEach3(::itemHandler)

    // equivalent to
    for (item in list) {
        itemHandler(item)
    }

}

fun itemHandler(item: Int) {
    if (item % 2 == 0) {
        return
    }
}

fun <T> List<T>.myForEach1(block: (T) -> Unit) {
    for (item in this) {
        block(item)
    }
}


inline fun <T> List<T>.myForEach2(block: (T) -> Unit) {
    for (item in this) {
        block(item)
    }
}
inline fun <T> List<T>.myForEach3(noinline block: (T) -> Unit) {
    for (item in this) {
        block(item)
    }
}


inline fun foo1(crossinline f: () -> Unit) {
    nonInlineFunction {
        f()
    }
}

fun nonInlineFunction(f: () -> Unit) {
    f()
}

fun testCrossInline(x: Int) {
    foo1 {
        if (x == 10) {
            return@foo1
        }
        println(x)
    }
}



inline fun <T> List<T>.myExists(predicate: (T) -> Boolean): Boolean {
    myForEach2 {
        if (predicate(it)) {
            return true
        }
    }
    return false
}

fun <T> List<T>.myExistsx(predicate: (T) -> Boolean): Boolean {
    myForEach2 {
        if (predicate(it)) {
            return true
        }
    }
    return false
}