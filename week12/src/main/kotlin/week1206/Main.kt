package com.javadude.week1206

import aaa.Animal // compiler - if I say "Animal", I mean "aaa.Animal"
//import bbb.Animal // would cause conflict
import bbb.Animal as BAnimal // import alias - works!

// using two separate classes with the same name

fun main() {
    // can fully-qualify the names
    val animal1 = aaa.Animal("Scott")
    val animal2 = bbb.Animal(8675309)

    // can import one and qualify the other
    val animal1a = Animal("Scott")
    val animal2a = bbb.Animal(8675309)

    // can use import alias and direct names without packages
    val animal1b = Animal("Scott")
    val animal2b = BAnimal(8675309)



}