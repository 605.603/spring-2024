package com.javadude.week1204

import kotlin.properties.Delegates
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

// other property delegations

fun main() {
    // pretend I read values from a file into a Map
    val map = mutableMapOf<String, Any?>(
        "name" to "Scott",
        "age" to 57,
    )

    val p = Person(map)
    println(p.name)
    println(p.age)
    p.name = "Sparkly the Crow"
    println(p.name)
    println(map["name"])

    val p2 = Person2()
    p2.name = "Scott"
    p2.name = "Mike"
    p2.name = "Sue"

    p2.age = 10
    println(p2.age)
    p2.age = 20
    println(p2.age)
    p2.age = 30
    println(p2.age)
    p2.age = 40
    println(p2.age)
}

class Person(values: MutableMap<String, Any?>) {
    var name: String by values
    var age: Int by values
}

// similar to
//operator fun <PROPERTY_TYPE> Map<String, Any?>.getValue(thisRef: Any?, property: KProperty<*>): PROPERTY_TYPE {
//    @Suppress("UNCHECKED_CAST")
//    return this[property.name] as? PROPERTY_TYPE ?: throw IllegalArgumentException("Bad property")
//}


class Person2 {
    var name by Delegates.observable("") { property, oldValue, newValue ->
        println("${property.name} changed from $oldValue to $newValue")
    }
    var age by Delegates.vetoable(57) { property, oldValue, newValue ->
        println("${property.name} changing from $oldValue to $newValue")
        newValue != 30
    }
}

class Person3(
    val name1: String, // first name
    val name2: String, // last name
) {
    // delegation to property in the same class/object
    // mainly useful to rename properties
    val firstName by ::name1
    val lastName by ::name2
}

class Heart(
    val heartRate: Int
)

class Hand(
    val numberOfFingers: Int
)

class Person4(
    private val heart: Heart,
    private val hand: Hand,
) {
    // mainly useful to aggregate properties of child objects
    val heartRate by heart::heartRate
    val numberOfFingers by hand::numberOfFingers
}

class Person5(
    private val heart: Heart,
    private val hand: Hand,
) {
    val heartRate: Int
        get() = heart.heartRate
    val numberOfFingers
        get() = hand.numberOfFingers
}