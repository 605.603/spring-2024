package com.javadude.week0205

typealias SomethingDoer = (x: Int, y: Int) -> Unit

fun doStuff(
    x: Int,
    y: Int,
    doSomething: (x: Int, y: Int) -> Unit, // recommend this syntax
) {
    println("--------")
    println("x=$x, y=$y")
    doSomething(x, y)
    println("--------")
}

fun doStuff2(
    x: Int,
    y: Int,
    doSomething: SomethingDoer,
) {
    println("--------")
    println("x=$x, y=$y")
    doSomething(x, y)
    println("--------")
}



var z: Int = 0

fun main() {
    // what is happening here with z?

    // in kotlin - LAMBDAS ARE CLOSURES! They capture and can modify their environment
    doStuff(10, 20) { x, y ->
        println(x + y + z) // lambda has to know about z - IT'S A CLOSURE - doesn't use a copy of z
            // when lambda defined; gets its value when we eval expression
        z = 100
        // in Java - z would be treated as "effectively final"
        //   -- can't modify after anon inner class set up that uses it
        //   -- makes a copy of the value z, and won't let you modify z after
    }

    println(z)
}