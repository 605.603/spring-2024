package com.javadude.week0201

var x = 10
var y = 20
fun main() {
    // expression
    val result = if (x < 10) {
        "Low"
    } else if (y < 10) {
        "Medium"
    } else {
        "whatevs"
    }

    var resultA = ""
    if (x < 10) {
        resultA = "Low"
    } else if (y < 10) {
        resultA = "Medium"
    } else {
        resultA = "whatevs"
    }

    resultA = if (x < 10) {
        "Low"
    } else if (y < 10) {
        "Medium"
    } else {
        "whatevs"
    }

    println(result)

    // statement
    if (x < 10) {
        println("Low")
    } else if (y < 10) {
        println("Medium")
    } else {
        println("whatevs")
    }

    // expression
    val result2 = when {
        x < 10 -> "Low"
        y < 10 -> "Medium"
        else -> "Low"
    }

    // statement
    when {
        x < 10 -> println("Low")
        y < 10 -> println("Medium")
    }

    val result3 = when (x + y) {
        20 -> {
            println("value was 20")
            "foo" // last expression is value of the block
        }
        25 -> "fee"
        30 -> "fie"
        else -> "bye" // what if we wanted to print the value?
    }

    when (x + y) {
        20 -> {
            println("not what I was expecting")
            println("value was 20")
        }
        25 -> {
            println("not what I was expecting")
            println("value was 25")
        }
        else -> "bye"
    }

    val result4 = when (val z = x + y) {
        20 -> "foo"
        25 -> "fee"
        else -> "bye: value is $z"
    }

    println(result4)
}

fun foo() {
    x = 100
}

