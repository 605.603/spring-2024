package com.javadude.week0203

fun main() {
    val x = a()
    println(x)
    println(a())
    println(b1())
    println(b2())
    println(d1(1, 2))
    println(d2(1, 2))
    println(e1(1, 2, true, false, 10))

    println(g1("A"))
    println(g1("A", "B"))
    println(g1("A", "B", "C", "D", "E", "F"))

    // named parameters
    println(
        e1(
            a = 1,
            b = 2,
            isList = true,
            personExists = false,
            z = 10,
        )
    )
    println(
        f1(
            a = 1,
            b = 2,
            isList = true,
            personExists = false,
            z = 10,
        )
    )
    println(
        f1(
            a = 1,
            b = 2,
            isList = true,
            z = 10,
        )
    )
    println(
        f1(
            a = 1,
            b = 2,
        )
    )

    g4b("A", "B", "C", x="X", y="Y") // if varargs are early, need to name later params
    // recommend always put varargs last, esp for Java compatibility

}

// simple function with no params, return value - ONLY ONE RETURN VALUE
fun a(): Int {
    return 10
}

// single expression function
fun b1(): Int  = 10
fun b2()  = 10

var n = 20

// still single-expression function
fun c1() = if (n < 10) 10 else 20 // equiv to Java's ternary expression "n < 10 ? 10 : 20"
fun c2() =
    if (n < 10) {
        10
    } else {
        20
    }

fun c3(): Int {
    if (n < 10) {
        return 10
    } else {
        return 20
    }
}

fun c4(): Int {
    return if (n < 10) {
        10
    } else {
        20
    }
}
fun c5(): Int {
    return if (n < 10) 10 else 20
}

// parameters
fun d1(x: Int, y: Int) = x + y // single-expression function
    // NOTE - no comma after last parameter
// in Math: d(x, y) = x + y
fun d2(
    x: Int,
    y: Int, // multi-line parameters - always add , after last parameter
) = x + y // single-expression function

fun e1(
    b: Int,
    a: Int,
    isList: Boolean,
    personExists: Boolean,
    z: Int,
) = a + b // single-expression function

@JvmOverloads // generates equiv Java overload functions (same name diff arg list)
fun f1(
    // safest option - once you have an optional parameter, ONLY have optional after it
    a: Int,
    b: Int = 10, // Q: what will this generate in JVM code?
    isList: Boolean = false, // default value
    personExists: Boolean = true, // default value
    z: Int = 10, // default value
) = a + b // single-expression function

    // Java would have to define
    // f1(a, b, isList, personExists) calls f1(a, b, isList, personExists, 10)
    // f1(a, b, isList) calls f1(a, b, isList, true, 10)
    // f1(a, b) calls f1(a, b, false, true, 10)
    // f1(a) calls f1(a, 10, false, true, 10)

fun g1(vararg items: String): String {
    var result = ""
    for(item in items) {
        result += "$item,"
    }
    return result
}
fun g1a(x: String, y: String, z: String) {
    println(x)
    println(y)
    println(z)
}

fun g2(vararg items: String): String {
//    return g1(items) // won't work - type mismatch
    return g1(*items) // works - spreads the arguments out - spread operator - don't know number of items needed
}

fun g3(vararg items: String) {
//    g1a(*items) // won't work
    g1a(items[0], items[1], items[2]) // works - know number of items
}

fun g4a(x: Int, y: Int, vararg items: String) {

}
fun g4b(vararg items: String, x: String, y: String) {

}

// if need multiple "vararg" groups - must use data structures like list or array
fun g4c(names: List<String>, addresses: List<String>, x: Int, y: Int) {
    // call would look like
    // g4c(listOf("Scott", "Mike", "Steve"), listOf("123 Sesame", "456 Blunt St", "789 Whatzit"), 10, 20)
}

fun g4d(names: Array<String>) {
    g3(*names) // spread only works with Array to spread into vararg params
}

fun g4e(names: Array<String>) =
    g3(*names) // spread only works with Array to spread into vararg params

    // what is the type of g4e???
    // g3 doesn't return anything!

// DEFAULT RETURN TYPE IN KOTLIN IS Unit

fun sampleFunction1(): Unit { // you can explicitly set Unit as the return type
    // but it's never needed and you'll probably never see it
}
fun sampleFunction2() { // equivalent to sampleFunction1
}


// TODO fast fail
