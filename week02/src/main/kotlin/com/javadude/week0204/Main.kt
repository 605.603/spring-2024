package com.javadude.week0204

// CONCEPTS
// Template Method and Strategy Patterns
//    template method - algorithm with replaceable steps
//    strategy - passing in something to replace step with


fun printNames(vararg names: String) {
    for (name in names) { // redundant
        println( // redundant
            name // DIFFERENT
        ) // redundant
    } // redundant
}

fun printDoctors(vararg names: String) {
    for (name in names) { // redundant
        println( // redundant
            "Dr. $name" // DIFFERENT
        ) // redundant
    } // redundant
}

// GOAL: create function that can keep all the commonality and take ANOTHER function to decide what to print

// NEED FUNCTIONAL TYPES to define parameter

fun printStuff( // this is a template method
    vararg names: String,
    getThingToPrint: (String) -> String, // this is a Strategy function
) {
    for (name in names) { // redundant
        println( // redundant
            getThingToPrint(name)
        ) // redundant
    } // redundant
}

fun printStuff2( // this is a template method
    vararg names: String,
    doBefore: (String) -> Unit = {}, // same as { name -> /* do nothing */ }
    doAfter: (String) -> Unit = {}, // same as { name -> /* do nothing */ }
    getThingToPrint: (String) -> String, // this is a Strategy function
) {
    for (name in names) { // redundant
        doBefore(name)
        println( // redundant
            getThingToPrint(name)
        ) // redundant
        doAfter(name)
    } // redundant
}

fun justPrintName(name: String) = name
fun printDoctorName(name: String) = "Dr. $name"

// in math we can compose functions:
//   f(g(10))
//   f(n) = f'(g(n))
//   f(n, g) = g(n)

// HIGHER-ORDER FUNCTIONS
//   1 - pass functions as parameters, AND/OR
//   2 - return function as result

fun main() {
    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = ::justPrintName, // function reference
    )
    printStuff(
        "Scott", "Mary", "Steve", "Sue", // function reference
        getThingToPrint = ::printDoctorName,
    )

    // if we're only ever using the functions justPrintName and printDoctorName ONCE,
    // this is a good bit extra to define. we can shorten this!

    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { name: String -> "Dr. $name"}, // defining a function INLINE
            // AKA "lambda" - unnamed function
    )
    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { name: String -> "$name is just a person"}, // defining a function INLINE
            // AKA "lambda" - unnamed function
    )
    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { name: String ->
            val doctorName = "Dr. $name"
            println("DEBUG: $doctorName")
            doctorName
        },
    )
    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { name -> "$name is just a person"}, // can infer type names
            // AKA "lambda" - unnamed function
    )

    // IF we only have a SINGLE argument, we don't have to specify its name - can just call it "it"

    printStuff(
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { "$it is just a person" }, // can infer type names - single arg as "it"
    )

    // If LAST arg is a function you can move it outside parens
    // Shorthand for last-arg lambda
    printStuff("Scott", "Mary", "Steve", "Sue") {
        "$it is just a person"
    }

    printStuff("Scott", "Mary", "Steve", "Sue") { "$it is just a person" }

    printStuff("Scott", "Mary", "Steve", "Sue") { name ->
        "$name is just a person"
    }

    println("-----")
    printStuff2( // if calling with > 1 lambda
        "Scott", "Mary", "Steve", "Sue",
        getThingToPrint = { it },
        doBefore = { print("    ") },
        doAfter = { println() },
    )
    printStuff2( // if calling with > 1 lambda
        "Scott", "Mary", "Steve", "Sue",
        doBefore = { print("    ") },
        doAfter = { println() },
    ) { it } // not as readable - what is this lambda

    printStuff2("Scott", "Mary", "Steve", "Sue") { it } // uses default lambdas

}

