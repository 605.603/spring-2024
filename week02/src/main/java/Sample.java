import com.javadude.week0203.MainKt;


interface Foo {
    void go();
}




public class Sample {
    static void doSomething(Foo foo) { // template method (in Java) - foo is strategy
        System.out.println("=====");
        foo.go(); // replaceable step
        System.out.println("=====");
    }

    public static void main(String[] args) {
        MainKt.f1(10);
//        final int x = 10; // final means cannot change it
        int x = 10; // non-final means can change it

        doSomething(
                new Foo() {
                    @Override
                    public void go() {
                        System.out.println(x); // x is COPY of the value of x outside - COPIED WHEN ANON INNER CLASS IS CREATED
                    }
                }
        );

        //x = 20; // makes x non-final
        // if we don't have the above, x is EFFECTIVELY final (we don't change it) and can be used

        doSomething(
                () -> {
                    System.out.println(x); // x is COPY of the value of x outside
                }
        );

    }


}
