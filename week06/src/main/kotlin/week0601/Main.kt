package org.example.week0601

interface Terminator {
    fun sayIllBeBack()
    fun sayAreYouSarahConnor()
    fun walkMenacingly()
}

interface SarahConnor {
    fun scream()
    fun run()
    fun sayYoureTerminated()
}

class TerminatorScript(
    private val terminator: Terminator,
    private val sarahConnor: SarahConnor,
) {
    fun action() {
        terminator.walkMenacingly()
        terminator.sayAreYouSarahConnor()
        sarahConnor.scream()
        sarahConnor.run()
        terminator.walkMenacingly()
        terminator.sayIllBeBack()
        terminator.walkMenacingly()
        sarahConnor.sayYoureTerminated()
    }
}

class Ahnold: Terminator {
    override fun sayIllBeBack() {
//        TODO("Not yet implemented") - will crash if called - great placeholder
        println("Ahhl be bach")
    }

    override fun sayAreYouSarahConnor() {
        println("Ah yew sa-ah conn-uh")
    }

    override fun walkMenacingly() {
        println("(sound) dun dun dun duh dun dun")
    }
}

class PeeWee: Terminator {
    override fun sayIllBeBack() {
        println("AHLLL BE BACK!!!")
    }

    override fun sayAreYouSarahConnor() {
        println("(whiny) are you sarah connor? HEH HEH")
    }

    override fun walkMenacingly() {
        println("la la la la la la")
    }
}

class Linda: SarahConnor {
    override fun scream() {
        println("eek")
    }

    override fun run() {
        println("(sound) footfalls")
    }

    override fun sayYoureTerminated() {
        println("You're terminated %$#%$")
    }
}

fun main() {
    val ahnold = Ahnold()
    val ahnold2 = Ahnold()
    val ahnold3 = Ahnold()
    val ahnold4 = Ahnold()
    val linda = Linda()
    val peewee = PeeWee()

    val script1 = TerminatorScript(
        terminator = ahnold,
        sarahConnor = linda,
    )

    script1.action()

    val script2 = TerminatorScript(
        terminator = peewee,
        sarahConnor = linda,
    )

    script2.action()
}