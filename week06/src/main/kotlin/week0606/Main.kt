package org.example.week0606

import org.example.week0603.Person

// OLD VERSION OF BUTTONS

abstract class OldButton {
    abstract fun onClick()
    // other stuff to interpret user interaction
}

class MyButton1: OldButton() {
    override fun onClick() {
        println("user clicked the button")
    }
}

// DESIGN PATTERNS - COMMUNICATION OF INTENT

// OBSERVER PATTERN
//    OBSERVABLE - something that can be observed
//    OBSERVER - something that observes an observable (and is notified when something interesting happens)

// Button in a user interface
//   when the user clicks that button

// base traditional pattern
//    INTERFACE for observer
//    Observable tracks observers

fun interface ButtonClickListener { // SAM Interface
    fun buttonClicked() // SINGLE-ABSTRACT METHOD (SAM)
    fun helper() {} // default functions ok
} // equiv to () -> Unit

class Button1 {
    init {
        object: Thread() {
            override fun run() {
                sleep(3000)
                userClickedTheButton()
            }
        }.start()
    }


    private val buttonClickListeners = mutableListOf<ButtonClickListener>()
    fun addButtonClickListener(listener: ButtonClickListener) {
        buttonClickListeners.add(listener)
    }
    fun removeButtonClickListener(listener: ButtonClickListener) {
        buttonClickListeners.remove(listener)
    }

    private fun userClickedTheButton() {
        buttonClickListeners.forEach { it.buttonClicked() }
    }
}

class Button2 {
    init {
        object: Thread() {
            override fun run() {
                sleep(3000)
                userClickedTheButton()
            }
        }.start()
    }


    private val buttonClickListeners = mutableListOf<() -> Unit>()
    fun addButtonClickListener(listener: () -> Unit) {
        buttonClickListeners.add(listener)
    }
    fun removeButtonClickListener(listener: () -> Unit) {
        buttonClickListeners.remove(listener)
    }

    private fun userClickedTheButton() {
        // all of these are equiv ways to call the listeners
        buttonClickListeners.forEach { it.invoke() }
        buttonClickListeners.forEach { it() }
        buttonClickListeners.forEach { listener -> listener() }
    }
}

class Handlers {
    fun handleButtonClick() {
        println("button was clicked! yay!")
    }
}


// OBSERVABLE FUNCTIONS
//    higher-order functions
//      - limit thought to function parameters
//      - limit thought to "tell me when something happens"
fun createUi(
    user: Person,
    dataChanged: (Person) -> Unit, // observer
) {
    // create text fields
    // attach dataChanged to fields to catch text changes
}



fun main() {
//    val button = Button1()
//    button.addButtonClickListener(object: ButtonClickListener {
//        override fun buttonClicked() {
//            println("user clicked the button!")
//        }
//    })
//
//    button.addButtonClickListener { // functional interfaces
//        println("user clicked the button!")
//    }

    val button2 = Button2()
    val handlers = Handlers()

    button2.addButtonClickListener(handlers::handleButtonClick) // function ref to match functional type
}