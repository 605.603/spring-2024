package org.example.week0602

interface Terminator {
    fun sayIllBeBack()
    fun sayAreYouSarahConnor()
    fun walkMenacingly()
}

interface SarahConnor {
    fun scream()
    fun run()
    fun sayYoureTerminated()
}

class TerminatorScript(
    private val terminator: Terminator,
    private val sarahConnor: SarahConnor,
) {
    fun action() {
        terminator.walkMenacingly()
        terminator.sayAreYouSarahConnor()
        sarahConnor.scream()
        sarahConnor.run()
        terminator.walkMenacingly()
        terminator.sayIllBeBack()
        terminator.walkMenacingly()
        sarahConnor.sayYoureTerminated()
    }
}

object Ahnold: Terminator {
    override fun sayIllBeBack() {
//        TODO("Not yet implemented") - will crash if called - great placeholder
        println("Ahhl be bach")
    }

    override fun sayAreYouSarahConnor() {
        println("Ah yew sa-ah conn-uh")
    }

    override fun walkMenacingly() {
        println("(sound) dun dun dun duh dun dun")
    }
}

object PeeWee: Terminator {
    override fun sayIllBeBack() {
        println("AHLLL BE BACK!!!")
    }

    override fun sayAreYouSarahConnor() {
        println("(whiny) are you sarah connor? HEH HEH")
    }

    override fun walkMenacingly() {
        println("la la la la la la")
    }
}

object Linda: SarahConnor {
    override fun scream() {
        println("eek")
    }

    override fun run() {
        println("(sound) footfalls")
    }

    override fun sayYoureTerminated() {
        println("You're terminated %$#%$")
    }
}

fun main() {
    val script1 = TerminatorScript(
        terminator = Ahnold,
        sarahConnor = Linda,
    )

    script1.action()

    val script2 = TerminatorScript(
        terminator = PeeWee,
        sarahConnor = Linda,
    )

    script2.action()
}