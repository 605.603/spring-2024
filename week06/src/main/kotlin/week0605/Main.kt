package org.example.week0605

import com.jogamp.opengl.Threading.Mode

class MyList1: List<String> {
    // would need to implement everything!
    override val size: Int
        get() = TODO("Not yet implemented")

    override fun get(index: Int): String {
        TODO("Not yet implemented")
    }

    override fun isEmpty(): Boolean {
        TODO("Not yet implemented")
    }

    override fun iterator(): Iterator<String> {
        TODO("Not yet implemented")
    }

    override fun listIterator(): ListIterator<String> {
        TODO("Not yet implemented")
    }

    override fun listIterator(index: Int): ListIterator<String> {
        TODO("Not yet implemented")
    }

    override fun subList(fromIndex: Int, toIndex: Int): List<String> {
        TODO("Not yet implemented")
    }

    override fun lastIndexOf(element: String): Int {
        TODO("Not yet implemented")
    }

    override fun indexOf(element: String): Int {
        TODO("Not yet implemented")
    }

    override fun containsAll(elements: Collection<String>): Boolean {
        TODO("Not yet implemented")
    }

    override fun contains(element: String): Boolean {
        TODO("Not yet implemented")
    }
}

// explicit delegation functions
class MyList2: List<String> {
    private val realList = mutableListOf<String>()
    override val size: Int
        get() = realList.size

    override fun get(index: Int) = realList[index]
    override fun isEmpty() = realList.isEmpty()

    override fun iterator() = realList.iterator()
    override fun listIterator() = realList.listIterator()

    override fun listIterator(index: Int) = realList.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int) = realList.subList(fromIndex, toIndex)

    override fun lastIndexOf(element: String) = realList.lastIndexOf(element)

    override fun indexOf(element: String) = realList.indexOf(element)

    override fun containsAll(elements: Collection<String>) = realList.containsAll(elements)

    override fun contains(element: String) = realList.contains(element)
}

// using kotlin interface delegation
class MyList3: List<String> by mutableListOf<String>() // creates a list and all delegation functions

interface Printer {
    fun printStuff()
}

class MyPrinter: Printer {
    override fun printStuff() {
        println("HI HI HI")
    }

}

class MyList4:
    MutableList<String> by mutableListOf<String>(),
    Printer by MyPrinter()
// creates a list and all delegation functions

// delegation to existing instance
class MyList5(
    private val realList: MutableList<String>
): MutableList<String> by realList


// restrict the type
// ADAPTER PATTERN - changes the type of wrapped object
// DIFFERENT INTERFACE, same behavior
class LockedDownList(
    private val realList: MutableList<String>
): List<String> by realList

class Model {
    private val realList = mutableListOf<String>() // base property
    val lockedDownList = LockedDownList(realList) // derived property

    private val realList2 = mutableListOf<String>() // base property
    val myList6 = MyList6(realList) // derived property

    fun addStuff(vararg items: String) {
        items.forEach { realList.add(it) }
    }
}


// SAME INTERFACE - change parameters or return values
// DECORATOR PATTERN
// this one tweaks parameters to .add
class MyList6(
    private val realList: MutableList<String>
): MutableList<String> by realList {
    override fun add(element: String): Boolean {
        return realList.add("X${element}X")
    }

    override fun toString() = realList.toString()
    override fun hashCode() = realList.hashCode()
    override fun equals(other: Any?) = realList == other
}


fun main() {
    val list = MyList4()
    list.add("a")
    list.printStuff()

    val existingList = mutableListOf("A")
    val myList5 = MyList5(existingList)

    val model = Model()
//    model.lockedDownList[10]
//    model.lockedDownList[10] = "a" // doesn't work
    model.addStuff("a", "b", "c")

    model.myList6.add("Scott")
    println(model.myList6)

}

