package org.example.week0603

const val DEFAULT_AGE1 = 10 // any expression can use this, inside or outside the file
private const val DEFAULT_AGE2 = 10 // only expressions in this file can use this

class Dog(
    age: Int = DEFAULT_AGE2
)

data class Person(
    val firstName: String = DEFAULT_FIRST_NAME,
    val lastName: String,
    val age: Int = DEFAULT_AGE,
) {
    init {
        numberOfPeople++ // need to make threadsafe - later
            // ++ NOT ATOMIC
    }

    val fullName: String // simple derived property
        get() = "$firstName $lastName" // DON'T USE THIS DEF OF FULL NAME IN REAL LIFE

    companion object {
        const val DEFAULT_FIRST_NAME = "Scott"
        const val DEFAULT_AGE = 10 // any expression can use this - must be qualified
        private const val DEFAULT_AGE3 = 10 // ONLY Person can use it
        var numberOfPeople = 0
        fun Person(lastName: String): Person {
            return Person(DEFAULT_FIRST_NAME, lastName, DEFAULT_AGE)
        }
        operator fun invoke(lastName: String): Person {
            return Person(DEFAULT_FIRST_NAME, lastName, DEFAULT_AGE)
        }
        operator fun invoke(
            firstName: String,
            lastName: String,
            age: Int,
        ): Person {
            return Person("${firstName}XXX", lastName, age)
        }
    }

    // awkward side discussion (awkward because a person wouldn't have this)
    //    just talking about operator overloading here
    operator fun get(index: Int): Int {
        return 10
    }
}

fun foo(
    someFunction1: (String) -> Int, // someFunction1 cannot be null, takes String, returns Int
    someFunction2: (String) -> Int?, // someFunction2 cannot be null, takes String, returns nullable Int
    someFunction3: ((String) -> Int)?, // someFunction3 can be null, takes String, returns Int
) {
    println(someFunction1("Scott"))
    println(someFunction2("Scott"))
    println(someFunction3!!("Scott")) // DON'T DO THIS!!! (or at least 99.99999999999999999999999999999% of the time)
    println(someFunction3?.invoke("Scott")) // DON'T DO THIS!!! (or at least 99.99999999999999999999999999999% of the time)
}

fun main() {
    val pp = Person("", "", 10)
    println(pp)

    // operator overload example
    var p0: Person? = Person("", "", 10)
    println(p0?.get(10))

    println(Person.DEFAULT_FIRST_NAME)
    println(Person.numberOfPeople)
    val p1 = Person("a", "b", 10)
    println(Person.numberOfPeople)
    val p2 = Person("a", "b", 10)
    println(Person.numberOfPeople)
    val p3 = Person("a", "b", 10)
    println(Person.numberOfPeople)
    val p4 = Person("a", "b", 10)
    println(Person.numberOfPeople)

    val p5 = Person.Person("assadd")
    val p6 = Person("assadd")
    val p7 = Person.invoke("assadd")
}
