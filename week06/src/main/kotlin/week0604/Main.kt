package org.example.week0604

//class Doctor
//abstract class Mammal

//class Human: Mammal(), Doctor() // won't work - single superclass only!

// CLASSES - "isA" relationship
// INTERFACE - "canDo" relationship, traits, way to communicate

interface Doctor {
    fun prescribeMedication()
}

abstract class Mammal

class Human: Mammal(), Doctor {
    override fun prescribeMedication() {
        println("scribble on paper")
    }
}
class Cat: Mammal(), Doctor {
    override fun prescribeMedication() {
        println("scribble on paper")
    }
}

class Dog: Mammal(), Doctor {
    override fun prescribeMedication() {
        println("scribble on paper")
    }
}

// mix-in v1 - use default functions/properties
interface Doctor2 {
    var x: Int // abstract
//    var y: Int = 10 // concrete - no can do! no backing fields allowed in interface
    val z: Int // properties w/o backing fields are ok!
        get() = 10
    fun prescribeMedication() {
        println("scribble on paper: $x $z")
    }
}

abstract class Mammal2

class Human2: Mammal2(), Doctor2 {
    override var x: Int = 10
}
class Cat2: Mammal2(), Doctor2 {
    override var x: Int = 20
}

class Dog2: Mammal2(), Doctor2 {
    override var x: Int = 30
    override val z: Int = 45

    override fun prescribeMedication() {
        println("scribble on paper, eat it")
    }
}
