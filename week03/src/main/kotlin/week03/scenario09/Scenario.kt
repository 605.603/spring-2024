package week03.scenario09

import kotlin.reflect.KClass

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

open class Toolbox {
    var tools = emptyList<Tool>()
        private set

    var adders: MutableMap<KClass<*>, (Tool, List<Tool>) -> List<Tool>> = mutableMapOf()
        private set

    fun registerAdder(toolType: KClass<*>, adder: (Tool, List<Tool>) -> List<Tool>) {
        adders[toolType] = adder
    }

    fun add(tool: Tool) {
        tools = adders[tool::class]?.invoke(tool, tools) ?: tools
    }
}

fun main() {
    val toolbox: Toolbox = Toolbox()

    toolbox.registerAdder(Saw::class) { tool, tools ->
        println("Calling Saw adder")
        tools + tool
    }

    toolbox.registerAdder(Screwdriver::class) { tool, tools ->
        println("Calling Screwdriver adder")
        tools + tool
    }

    println("adding tools")

    toolbox.add(Saw())
    toolbox.add(Screwdriver())
    println(toolbox.tools)
}