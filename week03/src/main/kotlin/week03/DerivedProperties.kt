package week03

class PersonDerived1(
    val firstName: String,
    val lastName: String,
    val age: Int,
) {
    fun getFullName() = "$firstName $lastName"
}

class PersonDerived2(
    val firstName: String,
    val lastName: String,
    val age: Int,
) {
    val fullName: String // derived property
        get() = "$firstName $lastName" // no backing field
            // string recreated every time
}

class PersonDerived3(
    val firstName: String,
    val lastName: String,
    val age: Int,
) {
    val fullName = "$firstName $lastName" // backing field
        // string created once
}

class PersonDerived4(
    var firstName: String,
    var lastName: String,
    val age: Int,
) {
    val fullName = "$firstName $lastName" // backing field - computed once. PROBLEM
        // if first or last changes, full name doesn't change
}

class PersonDerived5(
    var firstName: String,
    var lastName: String,
    val age: Int,
) {
    val fullName: String // derived property
        get() = "$firstName $lastName" // no backing field
    // string recreated every time - good b/c first or last name can change
}

fun main() {
    val personDerived1 = PersonDerived1("Scott", "Stanchfield", 57)
    println(personDerived1.getFullName())

    val personDerived2 = PersonDerived2("Scott", "Stanchfield", 57)
    println(personDerived2.fullName)
}