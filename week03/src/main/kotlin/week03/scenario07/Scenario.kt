package week03.scenario07

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

open class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: Screwdriver) { // B
        println("    Toolbox.add(Screwdriver)")
        tools = tools + tool
    }

    open fun add(tool: Saw) { // C
        println("    Toolbox.add(Saw)")
        tools = tools + tool
    }
}

class SafeToolbox: Toolbox() {
    override fun add(tool: Saw) { // D
        println("    SafeToolbox.add(Saw) - adding safely!!!")
        super.add(tool)
    }
}

fun main() {
    val safeToolbox: SafeToolbox = SafeToolbox()
    println("adding tools")

    safeToolbox.add(Saw()) // what will be called - add(Saw)
    safeToolbox.add(Screwdriver())
    println(safeToolbox.tools)

    // NOTE - calls toString() on each tool
    // toString() is intended for DEBUGGING, not to be user facing!
    //   would give you one true way to display the data
}