package week03.scenario02

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: Screwdriver) { // B
        println("    Toolbox.add(Screwdriver)")
        tools = tools + tool
    }
}


fun main() {
    val toolbox: Toolbox = Toolbox()
    val saw: Tool = Saw()
    val saw2: Saw = Saw()
    val screwdriver: Tool = Screwdriver()
    val screwdriver2: Screwdriver = Screwdriver()
    println("adding saw")
    toolbox.add(saw)
    println("adding screwdriver")
    toolbox.add(screwdriver) // what is called
        // compile time signature: Toolbox.add(Tool)
    println("adding screwdriver2")
    toolbox.add(screwdriver2) // what is called
        // compile time signature: Toolbox.add(Screwdriver)
    println("adding saw2")
    toolbox.add(saw2) // what is called
        // compile time signature: Toolbox.add(Tool)

    println(toolbox.tools)

    // NOTE - calls toString() on each tool
    // toString() is intended for DEBUGGING, not to be user facing!
    //   would give you one true way to display the data
}