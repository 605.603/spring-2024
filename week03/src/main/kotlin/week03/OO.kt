package week03

import kotlin.math.min

// three tenets of OO programming

//   Encapsulation
//      - hiding data
//      - protecting data

//   Inheritance
//     - creating more specific classes based on existing classes, possibly adding properties/functions
//     - Liskov substitutability

//   Polymorphism


private var amountOfPb = 32 // no control - could be a mistake anywhere in this file

fun removePb(amount: Int): Int {
    val removed = min(amount, amountOfPb)
    // problem - caller's responsibility
    amountOfPb -= removed
    return removed
}

// ENCAPSULATION
class PbJar {
     var amountOfPb = 32 // control!!!
        // default in kotlin is PUBLIC
        private set
        // get is public

    fun removePb(amount: Int): Int {
        val removed = min(amount, amountOfPb)
        amountOfPb -= removed
        return removed
    }
}

fun main1() {
    // encapsulation - great for control
    val jar = PbJar() // creates instance of PbJar
    println(jar.amountOfPb)
    println(jar.removePb(10))
    println(jar.amountOfPb)
    println(jar.removePb(10))
    println(jar.amountOfPb)
    println(jar.removePb(10))
    println(jar.amountOfPb)
    println(jar.removePb(10))
    println(jar.amountOfPb)
    println(jar.removePb(10))
    println(jar.amountOfPb)
}


// inheritance
// kingdom/phylim/class/order/family/genus/species

// kotlin design flaw - everything final/closed by default
open class Animal { // should be abstract - open makes it extendable
    var name: String = "Batman"
    fun run() {
        println("running")
    }
    open fun bloodTempRange(): IntRange {
        return 0..110
    }
}

abstract class Mammal: Animal() {
    // abstract - cannot create it - "concept"
    override fun bloodTempRange(): IntRange { // polymorphism - override superclass function/properties
        return 80..110
    }

    // override vs overload
    //   overload - different signature with same name - only parameters
    //      - reducing namespace/surface area of the type
    //   override - same name and parameters
    //      - changing behavior
}

class Cat: Mammal()
class Dog: Mammal()

fun main() {
    val animal: Animal = Cat()
    println(animal.name)
    animal.run()
    val cat: Cat = Cat()
    println(cat.name)
    cat.run()

    goAnimal(Dog())
    goAnimal(Cat())
//    goAnimal(Mammal()) // cannot create - abstract!
}

fun goAnimal(animal: Animal) {
    println(animal.name)
    animal.run()
}

