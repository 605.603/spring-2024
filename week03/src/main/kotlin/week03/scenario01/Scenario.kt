package week03.scenario01

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) {
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
}


fun main() {
    val toolbox: Toolbox = Toolbox()
    val saw = Saw()
    val screwdriver = Screwdriver()
    println("adding saw")
    toolbox.add(saw)
    println("adding screwdriver")
    toolbox.add(screwdriver)

    println(toolbox.tools)

    // NOTE - calls toString() on each tool
    // toString() is intended for DEBUGGING, not to be user facing!
    //   would give you one true way to display the data
}