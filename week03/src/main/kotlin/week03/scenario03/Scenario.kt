package week03.scenario03

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: Screwdriver) { // B
        println("    Toolbox.add(Screwdriver)")
        tools = tools + tool
    }
}


fun main() {
    val toolbox: Toolbox = Toolbox()
    val tools = listOf(Saw(), Screwdriver())
    println("adding tools")
    for(tool in tools) {
        toolbox.add(tool) // what is called here?
            // compile time signature is Toolbox.add(Tool)
    }

    println(toolbox.tools)

    // NOTE - calls toString() on each tool
    // toString() is intended for DEBUGGING, not to be user facing!
    //   would give you one true way to display the data
}