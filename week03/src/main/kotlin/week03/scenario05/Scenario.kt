package week03.scenario05

open class Tool {
    override fun toString(): String {
        return this::class.simpleName!! // almost NEVER use "!!" - we'll talk about this later with nullability
    }
}
class Screwdriver: Tool()
class Saw: Tool()

open class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: Screwdriver) { // B
        println("    Toolbox.add(Screwdriver)")
        tools = tools + tool
    }
}

class SafeToolbox: Toolbox() {
    fun add(tool: Saw) { // C
        println("    SafeToolbox.add(Saw) - adding safely!!!")
        super.add(tool)
    }
}

fun main() {
    val safeToolbox: Toolbox = SafeToolbox()
    val tools = listOf(Saw(), Screwdriver())
    println("adding tools")

    safeToolbox.add(Saw()) // what will be called - add(Tool)
    safeToolbox.add(Screwdriver())
    println(safeToolbox.tools)

    // NOTE - calls toString() on each tool
    // toString() is intended for DEBUGGING, not to be user facing!
    //   would give you one true way to display the data
}