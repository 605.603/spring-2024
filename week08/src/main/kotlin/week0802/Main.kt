package week0802

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.PrintWriter

fun main() {
    val file = File("hello.txt")

    // short-ish text - writeText is decent choice
    file.writeText("Scott")

//    file.writeText("""
    file.appendText("""
        Line 1
        Line 2
        Line 3
        Line 4
    """.trimIndent())

    val text = file.readText()
    println(text)

    // TYPES OF FILE WRITES/READS
    //    binary - InputStream, OutputStream
    //    text - Reader/Writer


    // THESE EXAMPLES ARE ALL EVIL. DO NOT DO THIS!!!
    // FILE MIGHT NOT GET CLOSED!!!!!!!!

    val file1 = File("File1.txt")
    val fw1 = FileWriter(file1)
    (1..100).forEach {
        fw1.write(it.toString())
        fw1.write("\n")
    }
    fw1.close() // if exception occurred while writing the file, this would not get called!!!

    val file2 = File("File2.txt")
    val fw2 = FileWriter(file2)
    val pw2 = PrintWriter(fw2)
    (1..100).forEach {
        pw2.println(it)
    }
    fw2.close() // if exception occurred while writing the file, this would not get called!!!

    val file3 = File("File3.bin")
    val fos3 = FileOutputStream(file3)
    (1..100).forEach {
        fos3.write(it)
    }
    fos3.close() // if exception occurred while writing the file, this would not get called!!!

    val fis3 = FileInputStream(file3)
//    repeat(100) {
//
//    }
    (1..100).forEach { _ ->
        println(fis3.read())
    }
    fis3.close() // if exception occurred while writing the file, this would not get called!!!

    // we'll fix the evil in the next file
}