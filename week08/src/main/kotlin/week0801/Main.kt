package week0801

// Exam stuff

abstract class Mammal(
    val name: String,
    val age: Int
)

class Dog(name: String, age: Int) : Mammal(name, age) {
    fun bark() { println("woof") }
}
class Cat(name: String, age: Int) : Mammal(name, age) {
    fun meow() { println("meow") }
}
class Platypus(name: String, age: Int) : Mammal(name, age) {
    fun waddle() { println("waddle waddle") }
}

fun main1() {
    val animals = listOf(
        Dog("Fido", 2),
        Dog("Rowlf", 4),
        Cat("Blackie", 3),
        Cat("Thunderbean", 4),
        Platypus("Roger", 6),
        Platypus("MacGuyver", 5),
    )

    animals
        .sortedBy { it.name }
        .forEach { println(it.name) }

    println(
        animals
            .sortedBy { it.name }
            .joinToString { it.name }
    )

    animals
        .filterIsInstance<Dog>()
        .forEach { it.bark() }

    println(
        animals.filterIsInstance<Platypus>().sumOf { it.age } +
            animals.filterIsInstance<Cat>().sumOf { it.age }
    )

    println(
        animals.filter { it is Platypus || it is Cat } .sumOf { it.age }
    )

    println(
        animals.filter { it !is Dog } .sumOf { it.age } // avoid!!!
    )
}


interface Employee {
    val name: String
    fun generateOrgChart() = name
}

interface Manager: Employee {
    val minions: List<Employee>
    override fun generateOrgChart() =
        "$name (${minions.joinToString { it.generateOrgChart() }})"
}

data class Henchman(override val name: String) : Employee

data class Boss(override val name: String, override val minions: List<Employee>) : Manager
data class CEO(override val name: String, override val minions: List<Employee>) : Manager

fun main() {
    val ceo =
        CEO(
            name = "Mr. Big",
            minions = listOf(
                Boss(name = "Boss 1",
                    minions = listOf(
                        Henchman("Thing 1"),
                        Henchman("Thing 2"),
                    )
                ),
                Boss(name = "Boss 2",
                    minions = listOf(
                        Henchman("Thing 3"),
                        Henchman("Thing 4"),
                    )
                )
            )
        )
    println(ceo.generateOrgChart())
    visit(ceo)
}

fun visit(employee: Employee) {
    visitImpl(employee)
    println()
}
fun visitImpl(employee: Employee) {
    print(employee.name)
    if (employee is Manager) {
        print(" (")
        employee.minions.forEachIndexed { index, minion ->
            if (index != 0) {
                print(", ")
            }
            visitImpl(minion)
        }
        print(")")
    }
}

fun foo2() {
    val x: String? = "Hello"
    x?.let {
        if (x is String) {
            println(x.toString())
        } else {
            println("x is not a String")
        }
    }
}


var x: String? = "Hello"
fun foo2a() {
    x?.let {
        if (x is String) {
            println(x.toString())
        } else {
            println("x is not a String")
        }
    }
}