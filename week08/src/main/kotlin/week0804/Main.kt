package week0804

import java.io.BufferedReader
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileReader
import java.io.FileWriter
import java.io.FilterWriter
import java.io.Writer

fun main() {
    UppercaseWriter(FileWriter("file6.txt")).use {
        it.write("""
            This is a test
            another line
            SOME UPPERCASE TEXT
        """.trimIndent())
    }

    UppercaseWriter(File("file7.txt").writer()).use {
        it.write("""
            Hello there!!!
            General Kenobi!
        """.trimIndent())
    }

    DataOutputStream(FileOutputStream("file8.bin")).use { dos ->
        dos.writeInt(42)
        dos.writeDouble(3.14159)
        dos.writeUTF("Hello there!")
    }
    DataInputStream(FileInputStream("file8.bin")).use { dis ->
        println(dis.readInt())
        println(dis.readDouble())
        println(dis.readUTF())
    }

    BufferedReader(FileReader("file7.txt")).use { br ->
        while(true) {
            val line = br.readLine() ?: break
            println(line)
        }
    }


}

class UppercaseWriter(
    realWriter: Writer
): FilterWriter(realWriter) {
    override fun write(c: Int) {
        super.write(c.toChar().uppercaseChar().code)
    }

    override fun write(str: String) {
        super.write(str.uppercase())
    }

    override fun write(cbuf: CharArray) {
        write(cbuf.toString())
    }

    override fun write(str: String, off: Int, len: Int) {
        super.write(str.uppercase(), off, len)
    }

    override fun write(cbuf: CharArray, off: Int, len: Int) {
        val newBuf = CharArray(len)
        cbuf.copyInto(newBuf, 0, off, off+len)
        newBuf.indices.forEach { n ->
            newBuf[n] = newBuf[n].uppercaseChar()
        }
        super.write(newBuf)
    }
}
