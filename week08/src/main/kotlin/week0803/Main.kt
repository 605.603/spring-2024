package week0803

import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.Writer

fun main() {
    val file1 = File("File1.txt")
    val fw1 = FileWriter(file1)
    fw1.ioStuff { writer ->
        (100..200).forEach {
            writer.write(it.toString())
            writer.write("\n")
        }
    }

    val file2 = File("File2a.txt")
    val fw2 = FileWriter(file2)
    fw2.use { writer ->
        (1..100).forEach {
            writer.write(it.toString())
            writer.write("\n")
        }
    }

    FileReader(file2).useLines { lines ->
        lines
            .filter { it.length > 1 }
            .forEach {
                println(it)
            }
    }
}


// in the olden days before exceptions, we used result codes

fun doA(): Int {
    // if everything ok, return 0
    // else return non-zero
    return 0
}
fun doB(): Int {
    // if everything ok, return 0
    // else return non-zero
    return 0
}
fun doC(): Int {
    // if everything ok, return 0
    // else return non-zero
    return 0
}
fun doD(): Int {
    // if everything ok, return 0
    // else return non-zero
    return 0
}

fun runEverything1() {
    if (doA() == 0) {
        if (doB() == 0) {
            if (doC() == 0) {
                if (doD() == 0) {
                    println("Success!")
                }
            }
        }
    }
}
fun runEverything2() {
    var rc = doA()
    if (rc == 0) {
        rc = doB()
    }
    if (rc == 0) {
        rc = doC()
    }
    if (rc == 0) {
        rc = doD()
    }

    if (rc == 0) {
        println("Success!")
    } else {
        println("Failure!")
    }
}


fun Writer.ioStuff(block: (Writer) -> Unit) {
    var pendingException: Throwable? = null
    try {
        block(this)

    } catch (e: Throwable) {
        pendingException = e

    } finally {
        try {
            close()
        } catch (e: Throwable) {
            throw pendingException ?: e
        } finally {
            pendingException?.let { throw pendingException }
        }
    }
}



var x = true

class SomeException: Exception()

fun doAWithExceptions() {
    // do something
    if (!x) {
        throw SomeException()
    }
}
fun doBWithExceptions() {
    // do something
    if (!x) {
        throw SomeException()
    }
}
fun doCWithExceptions() {
    // do something
    if (!x) {
        throw SomeException()
    }
}
fun doDWithExceptions() {
    // do something
    if (!x) {
        throw SomeException()
    }
}

fun doItAllWithExceptions() {
    doAWithExceptions()
    doBWithExceptions()
    doCWithExceptions()
    doDWithExceptions()
}
fun doItAllWithExceptionsWithCatch() {
    try {
        doAWithExceptions()
        doBWithExceptions()
    } catch (e: SomeException) {
        // report error but keep going
    }
    try {
        doCWithExceptions()
    } catch (e: SomeException) {
        // report error but keep going
        throw e
    }
    try {
        doDWithExceptions()
    } catch (e: SomeException) {
        // report error but keep going
    }
}

// IN KOTLIN, THERE ARE NO CHECKED EXCEPTIONS!!!
//    no "throws" clause on functions
// In java, some exceptions are "checked" exceptions
//    Throwable
//       Error
//       Exception
//          RuntimeException
// Checked exceptions
//    MUST catch the exception or declare it's thrown by the function