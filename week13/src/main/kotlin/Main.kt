package com.javadude

import com.javadude.sample.Repository
import kotlin.reflect.KClass

class Person

fun main() {
    val list = listOf("A", 1, 4.2, "B", Person())
    val newList = mutableListOf<String>()
    for (item in list) {
        if (item is String) {
            newList.add(item)
        }
    }
    println(newList)

    println(list.myFilterIsInstance1(String::class))
    println(list.myFilterIsInstance2<String>())

    val root = PersonNode("Scott", 57)
    root.insert(PlaceNode("Home", "123 Sesame"))
    root.insert(PersonNode("Mike", 10))
    root.insert(ToyNode("Games R Us", "Fun game"))
    root.insert(ToyNode("Games R Us", "Another Fun game"))
    root.insert(ToyNode("Evil Toys", "Sharp Knife... for kids!"))
    root.insert(PersonNode("Sue", 20))
    root.insert(PlaceNode("Google", "1600 Amphitheatre Parkway"))
    root.insert(ToyNode("Evil Toys", "Poison... for kids!"))
    root.insert(PersonNode("Trevor", 30))
    root.insert(PlaceNode("APL", "1111 Johns Hopkins Road"))

    root.inorder()
    println("-----")
    root.printInorder<PersonNode>()
    // root.inorder(PersonNode::class) // internal/publishedApi - can't call from outside an inline function


    println("=======")
    println("=======")
    val repo = Repository()
    repo.save(Person())
    repo.delete(Person())
    repo.count(5)
    println("=======")
    val wrapped = LoggingRepositoryWrapper(repo)
    wrapped.save(Person())
    wrapped.delete(Person())
    wrapped.count(5)
}

fun <T: Any> List<Any>.myFilterIsInstance1(clazz: KClass<T>): List<T> {
    val newList = mutableListOf<T>()
    for (item in this) {
        if (clazz.isInstance(item)) {
            @Suppress("UNCHECKED_CAST")
            newList.add(item as T)
        }
    }
    return newList
}

inline fun <reified T: Any> List<Any>.myFilterIsInstance2(): List<T> {
    val newList = mutableListOf<T>()
    for (item in this) {
        if (item is T) {
            newList.add(item)
        }
    }
    return newList
}

//tailrec fun xxx(n: Int) {
//    if (n > 0) {
//        xxx(n - 1)
//    }
//}

inline fun <reified T: Any> List<Any>.myFilterIsInstance3(): List<T> =
    myFilterIsInstance3a(T::class)

fun <T: Any> List<Any>.myFilterIsInstance3a(clazz: KClass<T>): List<T> {
    val newList = mutableListOf<T>()
    for (item in this) {
        if (clazz.isInstance(item)) {
            @Suppress("UNCHECKED_CAST")
            newList.add(item as T)
        }
    }
    return newList
}





open class Node(
    var name: String,
    var left: Node? = null,
    var right: Node? = null,
) {
    fun inorder() {
        left?.inorder()
        println(name)
        right?.inorder()
    }

    inline fun <reified T> printInorder() {
        inorder0(T::class)
    }

    @PublishedApi
    internal fun inorder0(clazz: KClass<*>) {
        left?.inorder0(clazz)
        if (clazz.isInstance(this)) {
            println(name)
        }
        right?.inorder0(clazz)
    }
    fun insert(newNode: Node) {
        if (newNode.name < name) {
            left?.insert(newNode) ?: run { left = newNode }
        } else {
            right?.insert(newNode) ?: run { right = newNode }
        }
    }
}

class PersonNode(
    name: String,
    val age: Int,
): Node(name)

class PlaceNode(
    name: String,
    val address: String,
): Node(name)

class ToyNode(
    val brand: String,
    name: String,
): Node(name)
