package com.javadude

typealias Color1 = String
    // just an alternative name for String

class Color(val value: String) {
    val lengthOfColorName: Int
        get() = value.length
}

@JvmInline
value class ColorVal(val value: String) {
    val lengthOfColorName: Int
        get() = value.length
}

fun foo() {
    explainPerson2("", "", 1, 2, 3, 4)
    explainPerson3("", Color(""), 1, 2, 3, 4)
    explainPerson4("", ColorVal(""), 1, 2, 3, 4)
}

fun explainPerson(
    name: String,
    favoriteColor: String,
    age: Int,
    numberOfFriends: Int,
    numberOfWorkingEyes: Int,
    height: Int,
) {

}

fun explainPerson2(
    name: String,
    favoriteColor: Color1,
    age: Int,
    numberOfFriends: Int,
    numberOfWorkingEyes: Int,
    height: Int,
) {

}

fun explainPerson3(
    name: String,
    favoriteColor: Color,
    age: Int,
    numberOfFriends: Int,
    numberOfWorkingEyes: Int,
    height: Int,
) {

}

fun explainPerson4(
    name: String,
    favoriteColor: ColorVal,
    age: Int,
    numberOfFriends: Int,
    numberOfWorkingEyes: Int,
    height: Int,
) {

}