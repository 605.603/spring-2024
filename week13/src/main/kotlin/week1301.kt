package com.javadude

import kotlin.reflect.KClass

fun main() {
    val x: List<String> = emptyList()

    val list = listOf("A", 1, 4.2, "B", Person())
    val newList = mutableListOf<String>()
    for (item in list) {
        if (item is String) {
            newList.add(item)
        }
    }
    println(newList)

    println(list.myFilterIsInstance3(String::class))
    println(list.myFilterIsInstance4<String>())
    // becomes this
//    val newList2 = mutableListOf<String>()
//    for (item in newList2) {
//        if (item is String) {
//            newList.add(item)
//        }
//    }
//    return newList2

}



fun <T: Any> List<Any>.myFilterIsInstance3(clazz: KClass<T>): List<T> {
    val newList = mutableListOf<T>()
    for (item in this) {
        if (clazz.isInstance(item)) {
            @Suppress("UNCHECKED_CAST")
            newList.add(item as T)
        }
    }
    return newList
}

inline fun <reified T: Any> List<Any>.myFilterIsInstance4(): List<T> {
    val newList = mutableListOf<T>()
    for (item in this) {
        if (item is T) {
            newList.add(item)
        }
    }
    return newList
}


