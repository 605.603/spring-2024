package com.javadude.sample

import com.javadude.Person
import com.javadude.annotation.GenerateLogging

@GenerateLogging("LoggingRepositoryWrapper")
class Repository {
    fun save(person: Person) {
        println("saved")
    }
    fun delete(person: Person) {
        println("deleted")
    }
    fun count(n: Int) {
        println("counting")
    }

    private fun foo() {}
}
