package com.javadude.logging.processor

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.getDeclaredFunctions
import com.google.devtools.ksp.isConstructor
import com.google.devtools.ksp.isPublic
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSFunctionDeclaration
import com.javadude.annotation.GenerateLogging
import java.io.OutputStreamWriter

class GenerateLoggingProcessor(
    val codeGenerator: CodeGenerator,
    val logger: KSPLogger,
    val options: Map<String, String>
) : SymbolProcessor {
    override fun process(resolver: Resolver): List<KSAnnotated> {
        // KEEP IN MIND THIS CODE GENERATOR IS WAAAAAAY OVER-SIMPLIFIED
        // FOR A LOT MORE DETAIL, SEE
        //     https://proandroiddev.com/so-how-do-i-write-a-kotlin-symbol-processor-ksp-b9606e9e3818
        // (it has some Android specifics but not much)
        resolver
            .getSymbolsWithAnnotation("com.javadude.annotation.GenerateLogging")
            .filterIsInstance<KSClassDeclaration>()
            .forEach { classDeclaration ->
                val packageName = classDeclaration.packageName.getQualifier()
                @OptIn(KspExperimental::class)
                val annotation = classDeclaration.getAnnotationsByType(GenerateLogging::class).first()

                OutputStreamWriter(
                    codeGenerator.createNewFile(
                        Dependencies(false),
                        packageName,
                        annotation.name,
                    )
                ).use { output ->
                    output.write(
                        """|package $packageName
                           |
                           |class ${annotation.name}(
                           |    val wrapped: ${classDeclaration.qualifiedName?.asString()}
                           |) {
                           |${classDeclaration.functions}
                           |}
                        """.trimMargin())
                }
            }

        return emptyList()
    }
}

val KSClassDeclaration.functions: String
    get() {
        return getDeclaredFunctions()
            .filter { it.isPublic() }
            .filter { !it.isConstructor() }
            .joinToString("\n") { function ->
                val functionName = function.simpleName.asString()
                """|    fun $functionName(${function.parameterDecls}) {
                   |        println("Calling $functionName")
                   |        wrapped.$functionName(${function.parameterNames})
                   |    }
                """.trimMargin()
            }
    }

val KSFunctionDeclaration.parameterDecls: String
    get() {
        return parameters
            .joinToString { parameter ->
                "${parameter.name?.getShortName()}: ${parameter.type.resolve().declaration.qualifiedName?.asString()}"
            }
    }

val KSFunctionDeclaration.parameterNames: String
    get() {
        return parameters
            .joinToString { parameter ->
                parameter.name?.getShortName() ?: "oops"
            }
    }



// GENERATED CODE SHOULD LOOK LIKE
// class LoggingRepositoryWrapper(
//    val wrapped: Repository
//) {
//    fun save(person: Person) {
//        println("running save")
//        wrapped.save(person)
//    }
//    fun delete(person: Person) {
//        println("running delete")
//        wrapped.delete(person)
//    }
//}