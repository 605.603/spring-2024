plugins {
    kotlin("jvm") version "1.9.23"
    id("com.google.devtools.ksp") version "1.9.23-1.0.20"
}

buildscript {
    dependencies {
        classpath(kotlin("gradle-plugin", version = "1.9.23"))
    }
}

group = "com.javadude"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

// A uses api(C)
// B uses implementation(A)
//    B can use everything public in A AND C

// A uses implementation(C)
// B uses implementation(A)
//    B can use everything public in A

// NEARLY ALL OF THE TIME, USE IMPLEMENTATION


dependencies {
    ksp(project(":loggingprocessor"))
    implementation(project(":annotation"))
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}