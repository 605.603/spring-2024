package week1009

sealed interface Events
data object ElectricOn : Events
data object ElectricOff : Events
data object Clap : Events

sealed interface States
data object Powered : States
data object LightsOn : States
data object NotPowered : States

fun main() {
    val machine = stateMachine {
        startState = NotPowered
        NotPowered.create {
            ElectricOn goesTo Powered
        }
        Powered.create {
            ElectricOff goesTo NotPowered
            Clap goesTo LightsOn andDoes { println("Clap!!!")}
        }
        LightsOn.create {
            ElectricOff goesTo NotPowered
            Clap goesTo Powered
        }
    }

    machine.handle(ElectricOn)
    machine.handle(ElectricOff)
    machine.handle(ElectricOn)
    machine.handle(Clap)
    machine.handle(Clap)
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) : StateMachine {
    val builder = StateMachineBuilder()
    builder.init()
    return builder.build()
}

class StateBuilder(stateId : States) {
    private val state = StateWithTransitions(stateId)
    infix fun Events.goesTo(stateId : States) : TransitionThing {
        state.transitions[this] = stateId
        return TransitionThing(state, this)
    }
    fun build() = state
}

class StateMachineBuilder {
    private val machine = StateMachine()
    var startState : States? = null

    fun States.create(init : StateBuilder.()->Unit) {
        val builder = StateBuilder(this)
        builder.init()
        val state = builder.build()
        machine.states[state.stateId] = state
    }
    fun build() = machine.apply { currentState = startState }
}




class StateMachine {
    val states = mutableMapOf<States, StateWithTransitions>()
    var currentState: States? = null

    fun add(stateWithTransitions: StateWithTransitions) {
        states[stateWithTransitions.stateId] = stateWithTransitions
    }
    fun handle(eventId: Events) {
        currentState?.let { stateId ->
            states[stateId]?.let  {stateWithTransitions ->
                stateWithTransitions.transitions[eventId]?.let {
                    stateWithTransitions.actions[eventId]?.invoke()
// basically equivalent to:
//                    stateWithTransitions.actions[eventId]?.let { action ->
//                        action()
//                    }
                    currentState = it
                    println("changed to state $it")
                }
            }
        }
    }

    override fun toString(): String {
        return "StateMachine(states=$states, currentState=$currentState)"
    }
}

class StateWithTransitions(val stateId: States) {
    val transitions = mutableMapOf<Events, States>()
    val actions = mutableMapOf<Events, () -> Unit>()

    infix fun Events.goesTo(stateId : States) : TransitionThing {
        transitions[this] = stateId
        return TransitionThing(this@StateWithTransitions, this)
    }

    override fun toString(): String {
        return "State(stateId=$stateId, transitions=$transitions)"
    }
}

class TransitionThing(val state : StateWithTransitions, val eventId : Events) {
    infix fun andDoes(action : () -> Unit) {
        state.actions[eventId] = action
    }
}
