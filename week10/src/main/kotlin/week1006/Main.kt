package com.javadude.week1006

// FIX THIS!!!

// Domain-Specific Languages (DSLs)
// internal - defined inside a programming language itself (Kotlin supports this)
// external - defined OUTSIDE programming language as a brand new language

enum class Events { ElectricOn, ElectricOff, Clap }
enum class States { NotPowered, Powered, LightsOn }


class StateMachine {
    val states = mutableMapOf<States, StateWithTransitions>()
    var currentState: States? = null

    fun handle(eventId: Events) {
        currentState?.let { stateId ->
            states[stateId]?.let  {stateWithTransitions ->
                stateWithTransitions.transitions[eventId]?.let {
                    stateWithTransitions.actions[eventId]?.invoke()
// basically equivalent to:
//                    stateWithTransitions.actions[eventId]?.let { action ->
//                        action()
//                    }
                    currentState = it
                    println("changed to state $it")
                }
            }
        }
    }

    override fun toString(): String {
        return "StateMachine(states=$states, currentState=$currentState)"
    }
}

class StateWithTransitions(val stateId: States) {
    val transitions = mutableMapOf<Events, States>()
    val actions = mutableMapOf<Events, () -> Unit>()

    override fun toString(): String {
        return "State(stateId=$stateId, transitions=$transitions)"
    }
}

fun main() {
    val machine =
        stateMachineBuilder()
            .stateMachine()
                .state(States.LightsOn)
                    .on(Events.ElectricOff)
                        .goto(States.NotPowered)
                    .on(Events.Clap)
                        .goto(States.Powered)
                .state(States.Powered)
                    .on(Events.ElectricOff)
                        .goto(States.NotPowered)
                    .on(Events.Clap)
                        .action { println("Clap!!!") }
                        .goto(States.LightsOn)
                .state(States.NotPowered)
                    .on(Events.ElectricOn)
                        .goto(States.Powered)
                .startState(States.NotPowered)

    println(machine)
    machine.handle(Events.ElectricOn)
    machine.handle(Events.ElectricOff)
    machine.handle(Events.ElectricOn)
    machine.handle(Events.Clap)
    machine.handle(Events.Clap)
}

fun stateMachineBuilder(): A {
    return StateMachineBuilder()
}

class StateMachineBuilder: A, B, C, D, E {
    private var machine: StateMachine? = null
    private var currentEventId: Events? = null
    private var currentTargetState: States? = null

    override fun action(action: () -> Unit): E {
        machine?.let { machine ->
            machine.states[currentTargetState]?.let { state ->
                currentEventId?.let { event ->
                    state.actions[event] = action
                }
            }
        }
        return this
    }

    override fun stateMachine(): B {
        machine = StateMachine()
        return this
    }

    override fun state(stateId: States): C {
        machine?.let { machine ->
            currentTargetState = stateId
            machine.states[stateId] = StateWithTransitions(stateId)
        }
        return this
    }

    override fun on(eventId: Events): D {
        currentEventId = eventId
        return this
    }

    override fun goto(targetStateId: States): C {
        machine?.let { machine ->
            currentEventId?.let { eventId ->
                currentTargetState?.let { stateId ->
                    machine.states[stateId]?.let { state ->
                        state.transitions[eventId] = targetStateId
                    }
                }
            }
        }
        return this
    }

    override fun startState(stateId: States): StateMachine {
        requireNotNull(machine)
        machine?.currentState = stateId
        return machine!!
    }
}


interface A {
    fun stateMachine(): B
}

interface B {
    fun state(stateId: States): C
    fun startState(stateId: States): StateMachine
}

interface C {
    fun state(stateId: States): C
    fun on(eventId: Events): D
    fun startState(stateId: States): StateMachine
}

interface D {
    fun goto(stateId: States): C
    fun action(action: () -> Unit): E
}

interface E {
    fun goto(stateId: States): C
}
