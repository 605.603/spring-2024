package com.javadude.week1004

// Domain-Specific Languages (DSLs)
// internal - defined inside a programming language itself (Kotlin supports this)
// external - defined OUTSIDE programming language as a brand new language

enum class Events { ElectricOn, ElectricOff, Clap }
enum class States { NotPowered, Powered, LightsOn }


class StateMachine {
    val states = mutableMapOf<States, StateWithTransitions>()
    var currentState: States? = null
    var currentEventId: Events? = null
    var currentTargetState: States? = null

    fun handle(eventId: Events) {
        currentState?.let { stateId ->
            states[stateId]?.let  {stateWithTransitions ->
                stateWithTransitions.transitions[eventId]?.let {
                    currentState = it
                    println("changed to state $it")
                }
            }
        }
    }

    fun state(stateId: States): StateMachine {
        currentTargetState = stateId
        states[stateId] = StateWithTransitions(stateId)
        return this
    }

    fun on(event: Events): StateMachine {
        currentEventId = event
        return this
    }

    fun goto(targetStateId: States): StateMachine {
        currentEventId?.let { eventId ->
            currentTargetState?.let { stateId ->
                states[stateId]?.let { state ->
                    state.transitions[eventId] = targetStateId
                }
            }
        }
        return this
    }

    fun startState(stateId: States): StateMachine {
        currentState = stateId
        return this
    }

    override fun toString(): String {
        return "StateMachine(states=$states, currentState=$currentState)"
    }
}

class StateWithTransitions(val stateId: States) {
    val transitions = mutableMapOf<Events, States>()
    override fun toString(): String {
        return "State(stateId=$stateId, transitions=$transitions)"
    }
}

fun stateMachine(): StateMachine {
    return StateMachine()
}




fun main() {
    val machine = stateMachine()
        .state(States.LightsOn)
            .on(Events.ElectricOff)
                .goto(States.NotPowered)
            .on(Events.Clap)
                .goto(States.Powered)
        .state(States.Powered)
            .on(Events.ElectricOff)
                .goto(States.NotPowered)
            .on(Events.Clap)
                .goto(States.LightsOn)
        .state(States.NotPowered)
            .on(Events.ElectricOn)
                .goto(States.Powered)
        .startState(States.NotPowered)

    println(machine)
    machine.handle(Events.ElectricOn)
    machine.handle(Events.ElectricOff)
    machine.handle(Events.ElectricOn)
    machine.handle(Events.Clap)
    machine.handle(Events.Clap)
}