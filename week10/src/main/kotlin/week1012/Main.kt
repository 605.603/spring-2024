package com.javadude.week1012

// This is a wrapper object that contains an Int - extra memory required
class Clock1(
    private val minutes: Int
) {
    fun printTime() {
        val hours = minutes / 60
        val mins = minutes % 60
        println("$hours:$mins")
    }
}

// This one is compiled to just use the Int directly
@JvmInline
value class Clock2(
    private val minutes: Int
) {
    fun printTime() {
        val hours = minutes / 60
        val mins = minutes % 60
        println("$hours:$mins")
    }
}

fun main() {
    val clock1 = Clock1(1000)
    val clock2 = Clock1(1000)
    clock1.printTime()
    clock2.printTime()
}