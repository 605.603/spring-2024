package com.javadude.week1002

sealed class Suit(
    val fancyName: String,
    val rank: Int,
) {
    fun trumps(other: Suit): Boolean = rank < other.rank
}

data object Spades: Suit("Spades ♠", 1) {
    fun a() { println("a") }
}
data object Hearts: Suit("Hearts ♥", 2)
data object Diamonds: Suit("Diamonds ♦", 3) {
    fun b() { println("b") }
}
data object Clubs: Suit("Clubs ♣", 4)

val suits = listOf(Spades, Hearts, Diamonds, Clubs)
fun main() {
    val suit: Suit = Spades
    val suit2 = Clubs
    println(suit)
    println(suit.rank)
    println(suit.fancyName)
    println(suits)
    println(suits[2])
    println(suit.trumps(suit2))

    when(suit) {
        Clubs -> println("")
        is Diamonds -> suit.b()
        Hearts -> println("")
        Spades -> (suit as Spades).a()
    }
}