package week1007

enum class Events { ElectricOn, ElectricOff, Clap }
enum class States { NotPowered, Powered, LightsOn }

fun main() {
    val machine = StateMachine().apply {
        currentState = States.NotPowered
        add(StateWithTransitions(States.NotPowered).apply {
            Events.ElectricOn goesTo States.Powered
            // EQUIVALENT TO Events.ElectricOn.goesTo(States.Powered)
        })
        add(StateWithTransitions(States.Powered).apply {
            Events.ElectricOff goesTo States.NotPowered
            Events.Clap goesTo States.LightsOn andDoes { println("Clap!!!")}
        })
        add(StateWithTransitions(States.LightsOn).apply {
            Events.ElectricOff goesTo States.NotPowered
            Events.Clap goesTo States.Powered
        })
    }

    machine.handle(Events.ElectricOn)
    machine.handle(Events.ElectricOff)
    machine.handle(Events.ElectricOn)
    machine.handle(Events.Clap)
    machine.handle(Events.Clap)
}

class StateMachine {
    val states = mutableMapOf<States, StateWithTransitions>()
    var currentState: States? = null

    fun add(stateWithTransitions: StateWithTransitions) {
        states[stateWithTransitions.stateId] = stateWithTransitions
    }
    fun handle(eventId: Events) {
        currentState?.let { stateId ->
            states[stateId]?.let  {stateWithTransitions ->
                stateWithTransitions.transitions[eventId]?.let {
                    stateWithTransitions.actions[eventId]?.invoke()
// basically equivalent to:
//                    stateWithTransitions.actions[eventId]?.let { action ->
//                        action()
//                    }
                    currentState = it
                    println("changed to state $it")
                }
            }
        }
    }

    override fun toString(): String {
        return "StateMachine(states=$states, currentState=$currentState)"
    }
}

class StateWithTransitions(val stateId: States) {
    val transitions = mutableMapOf<Events, States>()
    val actions = mutableMapOf<Events, () -> Unit>()

    infix fun Events.goesTo(stateId : States) : TransitionThing {
        transitions[this] = stateId
        return TransitionThing(this@StateWithTransitions, this)
    }

    override fun toString(): String {
        return "State(stateId=$stateId, transitions=$transitions)"
    }
}

class TransitionThing(val state : StateWithTransitions, val eventId : Events) {
    infix fun andDoes(action : () -> Unit) {
        state.actions[eventId] = action
    }
}
