package com.javadude.week1011

sealed interface Events
data object ElectricOn : Events
data object ElectricOff : Events
data object Clap : Events

sealed interface States
data object Powered : States
data object LightsOn : States
data object NotPowered : States

@DslMarker
annotation class Builder

fun main() {
    val machine = stateMachine {
        startState = NotPowered
        NotPowered create {
            ElectricOn goesTo Powered
        }
        Powered create {
            ElectricOff goesTo NotPowered
            Clap goesTo LightsOn andDoes { println("Clap!!!")}
        }
        LightsOn create {
            ElectricOff goesTo NotPowered
            Clap goesTo Powered
        }
    }

    machine handle ElectricOn
    machine handle ElectricOff
    machine handle ElectricOn
    machine handle Clap
    machine handle Clap
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) : StateMachine {
    val builder = StateMachineBuilder()
    builder.init()
    return builder.build()
}

@Builder
class StateBuilder(stateId : States) {
    private val state = StateWithTransitions(stateId)
    infix fun Events.goesTo(stateId : States) : TransitionThing {
        state.transitions[this] = stateId
        return TransitionThing(state, this)
    }
    fun build() = state
}

@Builder
class StateMachineBuilder {
    // changed to allow StateMachine to have immutable states and non-null current state
    private val states = mutableMapOf<States, StateWithTransitions>()
    var startState : States? = null

    infix fun States.create(init : StateBuilder.()->Unit) {
        val builder = StateBuilder(this)
        builder.init()
        val state = builder.build()
        states[state.stateId] = state
    }
    fun build() =
        startState?.let {
            StateMachine(states.toMap(), it)
        } ?: throw IllegalStateException("No start state defined. You're evil.")
}




class StateMachine(
    val states: Map<States, StateWithTransitions>, startState: States
) {
    var currentState: States = startState

    infix fun handle(eventId: Events) {
        states[currentState]?.let  {stateWithTransitions ->
            stateWithTransitions.transitions[eventId]?.let {
                stateWithTransitions.actions[eventId]?.invoke()
// basically equivalent to:
//                    stateWithTransitions.actions[eventId]?.let { action ->
//                        action()
//                    }
                currentState = it
                println("changed to state $it")
            }
        }
    }

    override fun toString(): String {
        return "StateMachine(states=$states, currentState=$currentState)"
    }
}

class StateWithTransitions(val stateId: States) {
    val transitions = mutableMapOf<Events, States>()
    val actions = mutableMapOf<Events, () -> Unit>()

    infix fun Events.goesTo(stateId : States) : TransitionThing {
        transitions[this] = stateId
        return TransitionThing(this@StateWithTransitions, this)
    }

    override fun toString(): String {
        return "State(stateId=$stateId, transitions=$transitions)"
    }
}

class TransitionThing(val state : StateWithTransitions, val eventId : Events) {
    infix fun andDoes(action : () -> Unit) {
        state.actions[eventId] = action
    }
}
