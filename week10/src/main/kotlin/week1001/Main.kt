package com.javadude.week1001

const val SPADES = 1
const val HEARTS = 2
const val DIAMONDS = 3
const val CLUBS = 4

val suit = 42

enum class Suit1 {
    SPADES, HEARTS, DIAMONDS, CLUBS
}
enum class Suit2(val fancyName: String) {
    SPADES("Spades ♠"),
    HEARTS("Hearts ♥"),
    DIAMONDS("Diamonds ♦"),
    CLUBS("Clubs ♣")
}

enum class Suit(val fancyName: String) {
    SPADES("Spades ♠"),
    HEARTS("Hearts ♥"),
    DIAMONDS("Diamonds ♦"),
    CLUBS("Clubs ♣");

    fun trumps(other: Suit): Boolean = ordinal < other.ordinal
}

fun main() {
    val suit = Suit.SPADES
    val suit2 = Suit.CLUBS
    println(suit)
    println(suit.ordinal)
    println(suit.name)
    println(suit.fancyName)
    println(Suit.entries)
    println(Suit.entries[2])
    println(suit.trumps(suit2))


//    when(suit) {
//        Suit.SPADES -> TODO()
//        Suit.HEARTS -> TODO()
//        Suit.DIAMONDS -> TODO()
//        Suit.CLUBS -> TODO()
////        else -> throw UnknownThingException()
//    }
}