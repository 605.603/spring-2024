package week0506

fun main() {
    val nameList = listOf("Scott", "Mary", "Zack", "Charlie", "Claire")

    nameList
        .asSequence()
        .onEach { // runs lambda on each item
            println(it)
        }
        .sorted() // returns a new list with items sorted - only works for Comparable items
        .also { // runs lambda on the entire list
            println("---------")
        }
        .forEach {
            println(it)
        }

}