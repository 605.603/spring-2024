package week0501

data class Child(
    var name: String,
)

data class Person(
    var name: String,
    var age: Int,
    var child: Child? = null,
)

fun main() {
    val x = listOf("a", "b")
    println(x)

    val y = x + "c" // shallow copy of x - just copies pointers to items
    println(y)

    val list1 = listOf<String>() // empty immutable List<String>
    val z = y + list1
    println(z)


    val personList1 = listOf(
        Person("Scott", 57),
        Person("Steve", 10),
    )

    println(personList1)

    val personList2 = personList1 + Person("Sue", 20)

    println(personList2)

    personList1[0].name = "Humuhumunukunukuapua'a"
    println(personList1)
    println(personList2)
    personList2[1].name = "Dory"
    println(personList1)
    println(personList2)

    val personList3 = listOf(
        personList1[0], // pointer to person at first pos in personList1
        personList1[1], // pointer to person at second pos in personList1
        Person("Sue", 20),
    )
    println(personList3)
    personList2[1].name = "Dude"
    println(personList3)

    val personList4 = listOf(
        personList1[0].copy(), // pointer to person at first pos in personList1
        personList1[1].copy(), // pointer to person at second pos in personList1
        Person("Sue", 20),
    )
    println(personList4)
    personList2[1].name = "Casper"
    println("---")
    println(personList2)
    println(personList4)

    // side track - pointers to other objects
    println("---")
    val childA = Child("Trevor")
    val parent1 = Person("Scott", 57, childA)
    val parent2 = Person("Nancy", 57, childA)

    println(parent1)
    println(parent2)

    childA.name = "Mr. Spock"
    println(parent1)
    println(parent2)

     // back to lists...
    println("---")
    println("---")
    println("---")
    val personList5 = listOf(
        Person("Gonzo", 30),
        Person("Kermit", 134),
        Person("Rowlf", 26),
    )
    println(personList5)

    val deepCopy1 = personList5.deepCopy1()
    println("deepCopy1 = $deepCopy1")
    personList5[0].name = "Piggy1"
    println("personList5 = $personList5")
    println("deepCopy1 = $deepCopy1")

    val deepCopy2 = personList5.deepCopy2()
    println("deepCopy2 = $deepCopy2")
    personList5[0].name = "Piggy2"
    println("personList5 = $personList5")
    println("deepCopy2 = $deepCopy2")

    val deepCopy3 = personList5.deepCopy3()
    println("deepCopy3 = $deepCopy3")
    personList5[0].name = "Piggy3"
    println("personList5 = $personList5")
    println("deepCopy3 = $deepCopy3")


    // mutable lists - advise against as much as possible
    val mutableList1 = mutableListOf(1, 2, 3)
    println(mutableList1)
    mutableList1.removeAt(0) // remove first item
    println(mutableList1)
    mutableList1.set(1, 10) // change second item
    println(mutableList1)
    mutableList1[1] = 20 // change second item
    println(mutableList1)
    mutableList1.add(42)
    println(mutableList1)

    println("=================================")
    println("=================================")
    // chaining operations
    val list2 = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    list2.forEach { // implicit use of "it" for single parameter
        println(it)
    }
    list2.forEach { item -> // explicitly name parameter
        println(item)
    }
    list2.forEach { _ -> // if not used, can specify "_"
        println("aaaa")
    }

    // forEach is terminal function - doesn't return a list to keep working on

    // create a chain with non-terminal functions
    // print odd numbers
    list2
        .filter { it % 2 == 1 } // % is "mod" - return a NEW LIST with just odd numbers
        .forEach { println(it) }

    // use a sequence to skip the list creation
    list2
        .asSequence() // creates a tour guide to walk through the items and pass each along
        .filter { it % 2 == 1 } // % is "mod" - passes on the item ONLY if it passes the test
        .forEach { println(it) }

    // to "just perform an operation" in the middle
    println("---")
    list2
        .asSequence() // creates a tour guide to walk through the items and pass each along
        .onEach { println("unfiltered: $it") }
        .filter { it % 2 == 1 } // % is "mod" - passes on the item ONLY if it passes the test
        .forEach { println("filtered: $it") }

    // great article on sequence or not: https://typealias.com/guides/when-to-use-sequences/

    val found = list2.find { it > 4 } // find first element that passes
    println(found)

    list2.forEachIndexed { index, item ->
        println("$index: $item")
    }
    list2.indices.forEach { index -> println(index) } // indicies returns a Range

    // only want to work with the first 5 elements
    val result = list2
        .take(5) // returns first five elements in NEW LIST - works with sequences
        .takeLast(2) // returns list with only the last two element - DOES NOT work with sequences
        // at this point we have items that were at indices 3, 4
        .drop(1) // returns NEW LIST without first element - works with sequences
        // result should only be the item at index 4

    println(result)

    list2.takeWhile { it < 5 } // gets however many items from start of list
    list2.takeLastWhile { it < 5 } // gets however many items from end of list
    list2.dropWhile { it < 5 } // strips however many items from start of list
    list2.dropLastWhile { it < 5 } // strips however many items from end of list

    list2.reversed() // returns NEW LIST
    list2.asReversed() // returns a VIEW of the original list in opposite order

    val list3 = listOf(1, 2, 3, 1, 2, 3, 4, 5, 3, 4)
    println(list3.distinct())
    val list4 = listOf(5, 4, 1, 2, 3, 1, 2, 3, 4, 5, 3, 4)
    println(list4.distinct())

    // dealing with mutability
    // suppose we have a mutable list:
    val listMutable = mutableListOf(1,2,3)
    println(listMutable)
    listMutable.add(10)
    println(listMutable)

    // we pass the data to a function that takes the list as read-only
    // his view of the list is read only, but the list is really mutable
    //   he casts the list and modifies it because he read the caller code
    //   and assumes it's mutable
    evilDoer(listMutable)

    // nasty!
    // to protect it, we need to wrap it without mutation functions
    evilDoer(ImmutableListWrapper(listMutable))

    println(listMutable)
}

// our wrapper
//   it has a private property realList that points to the real list we're wrapping
//     - no one can access the real list directly
//   it only implements List<Int> - no mutator functions
//   it delegates all functions in List<Int> to the realList
class ImmutableListWrapper(private val realList: List<Int>): List<Int> by realList {
    // the "by" delegates all List<Int> function calls to the realList - basically:
    //    override fun get(index: Int): Int = realList.get(index)
    //    override val size: Int
    //        get() = realList.size
    //    etc
}

// evildoer looked at the code calling him and realizes
//   the list is actually mutable... and does evil by
//   casting it
fun evilDoer(list: List<Int>) {
    (list as MutableList<Int>).add(10)
}

// extension function - syntactic sugar
//   call as
//      val copiedPersonList = personList5.deepCopy()
//      the object pointed to by personList5 is RECEIVING the deepCopy "message"
//   receiver: List<Person> -- becomes an implicit parameter
//      deepCopy(this: List<Person>)
//   no explicit parameters
fun List<Person>.deepCopy1(): List<Person> {
    val newList = mutableListOf<Person>()
    forEach {
        newList.add(it.copy())
    }
    return newList
}

// NOTE: To ensure a list cannot be cast modified

fun List<Person>.deepCopy2(): List<Person> {
    return this.map { // creates a new list, applying the lambda to each element
        it.copy()
    }
}

fun List<Person>.deepCopy3() =
    map { // creates a new list, applying the lambda to each element
        it.copy()
    }

// basically what the copy function looks like
class Child2
class Person2(
    var name: String,
    var age: Int,
    var child: Child2,
) {
    fun copy(
        name: String = this.name,
        age: Int = this.age,
        child: Child2 = this.child,
    ) = Person2(name = name, age = age, child = child)
}