package week0502

import week0501.Person
import kotlin.math.max
import kotlin.math.min

abstract class Mammal(
    val name: String,
    val age: Int,
): Comparable<Mammal> {
    override fun compareTo(other: Mammal) =
        name.compareTo(other.name)

    override fun toString(): String {
        return name
    }
}
class Dog(name: String, age: Int): Mammal(name, age)
class Cat(name: String, age: Int): Mammal(name, age)
open class Human(name: String, age: Int): Mammal(name, age)
class Doctor(name: String, age: Int): Human(name, age)

fun main() {
    val nameList = listOf("Scott", "Mary", "Zack", "Charlie", "Claire")
    nameList
        .onEach { // runs lambda on each item
            println(it)
        }
        .sorted() // returns a new list with items sorted - only works for Comparable items
        .also { // runs lambda on the entire list
            println("---------")
        }
        .onEach {
            println(it)
        }

    // The following actually worked, but was hard to read in the output
    //    copied by itself into week506/Main.kt for clarity
    println("---------")
    println("---------")
    nameList
        .asSequence()
        .onEach { // runs lambda on each item
            println(it)
        }
        .sorted() // returns a new list with items sorted - only works for Comparable items
        .also { // runs lambda on the entire list
            println("---------")
        }
        .forEach {
            println(it)
        }

    println("---------")
    println("---------")

    val mammals = listOf(
        Dog("Fido", 12),
        Cat("Puffball", 4),
        Cat("The Thing", 82),
        Dog("Fifi", 5),
        Human("Scott", 57),
        Doctor("The Doctor", 999),
        Dog("Rex", 2),
    )

    mammals
        .filterIsInstance<Human>() // returns NEW LIST of JUST Humans (and subclasses)
        .onEach { println("Human: ${it.name}") }
        .filterIsInstance<Doctor>() // returns NEW LIST of JUST Doctors
        .onEach { println("Doctor: ${it.name}") }

    // sorting
    //    if objects in list implement Comparable, can use sorted()
    //    if objects don't implement Comparable
    //       can use sortedBy if a property is Comparable and that's the only sort key
    //       can use sortedWith to pass an algorithm for determining sort order (Comparator)
    println("---------")
    mammals
        .sortedBy { it.name } // can use sorted() if the object is Comparable
        .onEach { println("${it.name}: ${it.age}") }
        .also {
            println("---------")
        }
        .sortedBy { it.age }
        .onEach { println("${it.name}: ${it.age}") }

    println("---------")
    mammals
        .chunked(3)
        .forEach { // sublists of size specified or smaller
            println(it)
        }

    println("---------")
    mammals
        .windowed(3, 1)
        .forEach { // slide a window across the list
            println(it)
        }

    val numberList = listOf(1,2,3,4,5,6,7,8,9,10)
    println(numberList.sum())
    println(numberList.average())
    println(numberList.minOf { it })
    println(numberList.maxOf { it })

    println(mammals.maxOf { it.age })

    println(mammals.any { it.age > 30 }) // is there anything with age > 30
    println(mammals.all { it.age > 30 }) // does everything have age > 30
    println(mammals.none { it.age > 30 }) // does everything NOT have age > 30

    println("=======")

    val listOfNumberLists = listOf(
        generateNumbersStartingWith(1),
        generateNumbersStartingWith(10),
        generateNumbersStartingWith(20),
        generateNumbersStartingWith(30),
    )

    println(listOfNumberLists)

    println(listOfNumberLists.flatten())

    val mappedList1 =
        numberList.map {
            generateNumbersStartingWith(it)
        }
    println(mappedList1)
    val mappedList2 =
        numberList
            .map {
                generateNumbersStartingWith(it)
            }
            .flatten()
    println(mappedList2)
    val mappedList3 =
        numberList
            .flatMap {
                generateNumbersStartingWith(it)
            }
    println(mappedList3)
    val mappedList4 =
        numberList
            .filter { it != 5 }
            .flatMap {
                generateNumbersStartingWith(it)
            }
    println(mappedList4)
    val mappedList5 =
        numberList
            .mapNotNull {
                if (it != 5) {
                    generateNumbersStartingWith(it)
                } else {
                    null
                }
            }
            .flatten()
    println(mappedList5)

    val mammalNames = mammals.map { it.name }
    val mammalAges = mammals.map { it.age }
    val averageMammalAge =
        mammals
            .map { it.age }
            .average()
    val averageCatAge =
        mammals
            .filterIsInstance<Cat>()
            .map { it.age }
            .average()

    val alternateAgeSum1 =
        mammals
            .map { it.age }
            .reduce { accumulator, value -> // accumulator starts at 0
                accumulator + value
            }
    val alternateAgeSum2 =
        mammals
            .map { it.age }
            .fold(0) { accumulator, value -> // pass in initial accumulator value
                accumulator + value
            }

    val stats = mammals
        .map { it.age }
        .fold(Stats()) { stats, value ->
            stats.max = max(stats.max, value)
            stats.min = min(stats.max, value)
            stats.sum += value
            stats
        }
    println(stats)

    println(mammals.find { it.name == "Scott" }?.age ?: "Scott not found")  // type is Any

    val mammalsByName = mammals.groupBy { it.name }
    println(mammalsByName)
    println(mammalsByName["Scott"]?.get(0)?.age ?: "Scott not found")

    val mammalsByType = mammals.groupBy { it::class.simpleName }
    println(mammalsByType)

    println(mammalsByType[Cat::class.simpleName])
    println(mammalsByType[Dog::class.simpleName])
    println(mammalsByType[Human::class.simpleName])
    println(mammalsByType[Doctor::class.simpleName])
}

data class Stats(
    var min: Int = Int.MAX_VALUE,
    var max: Int = Int.MIN_VALUE,
    var sum: Int = 0,
)

fun generateNumbersStartingWith(n: Int) =
    mutableListOf(n, n+1, n+2)